
function usage(){
    printf "Utilisation du script :\n"
    printf " -c, --config               Configuration interactive de l'installation (urls, connexion DB, mot de passe LDAP)\n"
    printf " -p, --package              Exécution du 'maven clean package' \n"
    printf " -e, --env=INSTALL_TYPE     Type d'installation : dev|prod (par défaut=prod)\n"
    printf " -H, --host=PRODIGE_DOMAIN  Domaine principal de l'installation\n"
    printf " -D, --dbname=DB_NAME       Nom de la base de données à connecter\n"
    printf " -U, --dbuser=DB_USER_NAME  Utilisateur de connexion à la base de données\n"
    printf " -P, --dbpwd=DB_USER_PWD    Mot de passe de connexion à la base de données\n"
    printf " -h                         Affiche ce message.\n"
}
 

PACKAGE=0
WITH_CONFIG=0
CONFIG_ENV=prod

# Propriétés de configuration obligatoires et définissables par les options du script
# {
PRODIGE_DOMAIN=
OVERRIDES_JDBC_DATABASE=
OVERRIDES_JDBC_USERNAME=
OVERRIDES_JDBC_PASSWORD=
# }

options=$(getopt -o:cphe:H:D:U:P: -l help,config,package,env:,host:,dbname:,dbuser:,dbpwd: -- "$@")
if [[ ! $options ]]
then
    # something went wrong, getopt will put out an error message for us
    exit 1
fi

eval set -- $options
while [ $# -gt 0 ]
do
  case "$1" in
    (-h|--help)    usage;                      exit 0;;
    (-c|--config)  WITH_CONFIG=1;;
    (-p|--package) PACKAGE=1;;
    (-e|--env)     CONFIG_ENV=$2;              shift;;
    (-H|--host)    PRODIGE_DOMAIN=$2;          shift;;
    (-D|--dbname)  OVERRIDES_JDBC_DATABASE=$2; shift;;
    (-U|--dbuser)  OVERRIDES_JDBC_USERNAME=$2; shift;;
    (-P|--dbpwd)   OVERRIDES_JDBC_PASSWORD=$2; shift;;
    (*)            break;;
  esac
  shift
done

CURDIR=`pwd`

DEFAULT_CONFIG=${CURDIR}/config-overrides.properties.${CONFIG_ENV}

GENERATED_CONFIG=${CURDIR}/config-overrides.properties
GENERATED_OVERRIDES=${CURDIR}/config-overrides-prod.xml

INITIAL_CONFIG=${CURDIR}/src/main/webapp/WEB-INF/config-overrides.properties
INITIAL_OVERRIDES=${CURDIR}/src/main/webapp/WEB-INF/config-overrides-prod.xml

cp ${INITIAL_OVERRIDES} ${GENERATED_OVERRIDES}
cp ${INITIAL_CONFIG} ${GENERATED_CONFIG}
cp ${INITIAL_OVERRIDES} ${GENERATED_OVERRIDES}.initial
cp ${INITIAL_CONFIG} ${GENERATED_CONFIG}.initial
cp ${DEFAULT_CONFIG} ${DEFAULT_CONFIG}.last


if [[ -f ${DEFAULT_CONFIG} ]]
then
  source <(grep = ${DEFAULT_CONFIG})
fi;
if [[ "${WITH_CONFIG}" -eq "1" ]] 
then
  echo "Configuration de l'installation : "
fi;


old_IFS=$IFS  # sauvegarde du séparateur de champ  
IFS=$'\n'     # nouveau séparateur de champ, le caractère fin de ligne  
for line in `cat ${DEFAULT_CONFIG}`
do

  if [[ $line =~ ^([a-z_A-Z]+)=(.*)$ ]]
  then
    # nom de la variable de config
    varname=${BASH_REMATCH[1]}
    # valeur extraite de la lecture du fichier de config par défaut (donc déjà calculée, en cas de variables imbriquées)
    initial_value=${!varname}

    # evaluation dynamique de la valeur par défaut de config (pour prendre en compte les nouvelles valeurs affectées aux variables imbriquées)
    default_value=`eval "echo ${BASH_REMATCH[2]}"`

    VARNAME=`echo ${varname^^}`
    if [ -v "${VARNAME}" -a -n "${!VARNAME}" ]; then
      echo "Set ${varname} to ${!VARNAME}"
      default_value="${!VARNAME}"
    fi;

    # configuration nécessaire si aucun valeur n'est définit sur les paramètres obligatoires
    if [ -v "${VARNAME}" -a  -z "${default_value}" ]; then
      WITH_CONFIG=1
    fi;

    if [[ "${WITH_CONFIG}" -eq "1" ]] 
    then
      echo "${varname} [ ${default_value} ] ?"
      read value
      value=${value:-${default_value}}
    else
      value=${default_value}
    fi

    # génération de la config d'installation à intégrer à l'overlay
    sed -i 's/[$][{]'"${varname}"'[}]/'"${value/\//\\\/}"'/g' ${GENERATED_OVERRIDES}
    sed -i 's/[$][{]'"${varname}"'[}]/'"${value/\//\\\/}"'/g' ${GENERATED_CONFIG}

    # mémorisation de la dernière config d'installation (NB : conserve les variables imbriquées si les nouvelles valeurs correspondent à la formule)
    if [ "${value}" = "${default_value}" ]; then pattern="${initial_value}"; else pattern="${BASH_REMATCH[2]}"; fi
    pattern=`echo "${pattern/\//\\\/}"` && pattern=`echo "${pattern/[$]/[$]}"` && pattern=`echo "${pattern/[{]/[{]}"` && pattern=`echo "${pattern/[\}]/[\}]}"`
    sed -i 's/'"${varname}"'='"${pattern}"'/'"${varname}"'='"${value/\//\\\/}"'/g' ${DEFAULT_CONFIG}.last

    # change la valeur de la variable dans l'environnement bash
    eval ${varname}=${value}
  elif [[ "${WITH_CONFIG}" -eq "1" ]] 
  then
    echo $line
  fi;
done
IFS=${old_IFS}

echo ""
echo "Le fichier ${DEFAULT_CONFIG}.last a été généré avec les dernières valeurs de remplacement de l'installation."
echo "ASTUCE : Si c'est souhaité, le renommer en ${DEFAULT_CONFIG} pour qu'il devienne la config par défaut de l'installation "
echo ""

if [[ "${PACKAGE}" -eq "1" ]] 
then
  sudo systemctl stop tomcat8
  cd /home/devperso/tomcat/prodige41
  rm geosource
  cd ${CURDIR}
  cp ${GENERATED_CONFIG} ${INITIAL_CONFIG} && cp ${GENERATED_OVERRIDES} ${INITIAL_OVERRIDES} && ./mvnw clean package && mv ${GENERATED_CONFIG}.initial ${INITIAL_CONFIG} && mv ${GENERATED_OVERRIDES}.initial ${INITIAL_OVERRIDES}

  cd /home/devperso/tomcat/prodige41
  ln -s ${CURDIR}/target/geosource-overlay-3.8.3-0 geosource
else
  sudo systemctl stop tomcat8
  cp -R ${CURDIR}/src/main/webapp/* ${CURDIR}/target/geosource-overlay-3.8.3-0/.
  cp ${GENERATED_CONFIG} ${CURDIR}/target/geosource-overlay-3.8.3-0/WEB-INF/`basename ${INITIAL_CONFIG}`
  cp ${GENERATED_OVERRIDES} ${CURDIR}/target/geosource-overlay-3.8.3-0/WEB-INF/`basename ${INITIAL_OVERRIDES}`
fi;
cd ${CURDIR}
rm ${GENERATED_CONFIG}.initial
rm ${GENERATED_OVERRIDES}.initial
sudo systemctl restart tomcat8 && tail -f /var/log/tomcat8/catalina.out
