# Changelog

All notable changes to [jpr_prodige_geonetwork](https://gitlab.adullact.net/prodige/jpr_prodige_geonetwork) project will be documented in this file.

## [4.4.6](https://gitlab.adullact.net/prodige/jpr_prodige_geonetwork/compare/4.4.5...4.4.6) - 2024-05-06

## [4.4.5](https://gitlab.adullact.net/prodige/jpr_prodige_geonetwork/compare/4.4.4...4.4.5) - 2024-04-02

## [4.4.4](https://gitlab.adullact.net/prodige/jpr_prodige_geonetwork/compare/4.4.3...4.4.4) - 2023-06-14

### Changed

- allow synchro for all harvested non geo or geo dataset

## [4.4.3](https://gitlab.adullact.net/prodige/jpr_prodige_geonetwork/compare/4.4.2...4.4.3) - 2023-06-14

## [4.4.2](https://gitlab.adullact.net/prodige/jpr_prodige_geonetwork/compare/4.4.1...4.4.2) - 2023-06-14

## [4.4.1](https://gitlab.adullact.net/prodige/jpr_prodige_geonetwork/compare/4.4.0_rc9...4.4.1) - 2023-03-17

## [4.4.0_rc9](https://gitlab.adullact.net/prodige/jpr_prodige_geonetwork/compare/4.4.0-rc8...4.4.0_rc9) - 2022-12-19

