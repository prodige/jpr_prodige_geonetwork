# Module PRODIGE Geonetwork
Module de catalogage basé sur [Geonetwork](https://github.com/geonetwork)  
Surcouche intégrée par Overlay maven

## Installation
[documentation d'insatllation en développement](cicd/dev/README.md).

## Accès à l'application

[Accès au catalogue](https://catalogue.prodige.internal/geonetwork).