
## Run

Run with
```bash
docker-compose up
```
url : http://localhost:8080/geonetwork

Stop with
```bash
docker-compose down
```