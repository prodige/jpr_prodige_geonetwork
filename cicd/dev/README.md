
# README
<!-- TOC -->

- [README](#readme)
  - [Overview](#overview)
  - [Start developpement](#start-developpement)
    - [Download dependency](#download-dependency)
    - [Compile the war file](#compile-the-war-file)
    - [Deploy war](#deploy-war)
  - [Stop developpement](#stop-developpement)
  - [General Docker commands](#general-docker-commands)
  - [Advanced Docker commands](#advanced-docker-commands)
    - [Clean commands](#clean-commands)
    - [Monitoring commands](#monitoring-commands)

<!-- /TOC -->

## Overview

This file give commands to start development with docker.

Require:
 - Docker installed.
 - Docker well configured, cf. **/etc/docker/deamon.json**
 - User in docker group to avoid sudo/su right

## Start developpement

### Download dependency


Go to the **root directory** of this project and download geonetwork.war

```bash
mkdir -p source
cd source
wget -O geonetwork-3.10.4-0.war https://sourceforge.net/projects/geonetwork/files/GeoNetwork_opensource/v3.10.4/geonetwork.war/download
chmod o+r *.war
cd ..
```

### Compile the war file

Make these commands in the root directory of this project:

```bash
docker run -u root -w "/usr/local/tomcat/build" --volume "${PWD}:/usr/local/tomcat/build" tomcat:8-jdk8 bash -c "./mvnw install:install-file -DgroupId=org.geonetwork-opensource -DartifactId=geonetwork -Dversion=3.10.4-0 -Dpackaging=war -Dfile=./source/geonetwork-3.10.4-0.war && ./mvnw clean package"
```

### Deploy war

Make these commands in this directory the ```cd ./cicd/dev```

Run container
```bash
# Build dev image
docker-compose build

# Run containers
docker-compose up
```


The url web are: http://localhost:8080/geonetwork


## Stop developpement

Make these commands in this directory ```cd ./cicd/dev```.

```bash
# Shutdown containers
docker-compose down

# Remove dev image
docker rmi jpr_prodige_geonetwork_dev_web
```

## General Docker commands

| Description | Commandes |
|- |- |
| Display images            | ```docker images``` |
| Remove image              | ```docker rmi myimages``` |
|- |- |
| Display all running containers | ```docker ps``` |
| Display all containers    | ```docker ps -a``` |
| Remove container          | ```docker rm mycontainer``` |
| Display info container    | ```docker inpect mycontainer``` |
| Display container ouput   | ```docker logs mycontainer``` |
| Stop container            | ```docker stop mycontainer``` |
| Kill container            | ```docker kill mycontainer``` |
|- |- |
| Connection as root | ```docker exec --user 0 -w / -it jpr_prodige_geonetwork_dev_web bash``` |
| Connection as www-data | ```docker exec --user www-data -w /var/www/html -it jpr_prodige_geonetwork_dev_web bash``` |
| Connection as postgres | ```docker exec --user postgres -it tpl_alkante_symfony_3_dev_db bash``` |
| Run composer install | ```docker exec --user www-data -w /var/www/html -it jpr_prodige_geonetwork_dev_web composer install ``` |

## Advanced Docker commands


### Clean commands
```bash
# Stop all container
docker stop $(docker ps -a -q)

# Remove all container
docker rm $(docker ps -a -q)

# Delete all image with name or tag aas "<none>"
docker rmi `docker images| egrep "<none>" |awk '{print $3}'`
```

### Monitoring commands

```bash
# Watch all container
watch -n 1 "docker ps -a"

# Watch all images
watch -n 1 "docker images"

# Watch all resources container
docker stats
```
