#!/bin/sh
set -e

# Source env file
SCRIPT=`readlink -f $0`
SCRIPTDIR=`dirname $SCRIPT`
SCRIPTNAME=`basename $SCRIPT`
ROOTDIR="$SCRIPTDIR/../.."
. $SCRIPTDIR/env.sh


mkdir -p $ROOTDIR/source
if [ ! -f $ROOTDIR/source/geonetwork-3.10.4-0.war ]; then
  cd $ROOTDIR/source
  wget -nv -O geonetwork-3.10.4-0.war https://sourceforge.net/projects/geonetwork/files/GeoNetwork_opensource/v3.10.4/geonetwork.war/download
fi;
