#!/bin/sh
set -e

# Source env file
SCRIPT=`readlink -f $0`
SCRIPTDIR=`dirname $SCRIPT`
SCRIPTNAME=`basename $SCRIPT`
ROOTDIR="$SCRIPTDIR/../.."
. $SCRIPTDIR/env.sh


cd $ROOTDIR


# Build war
docker run -u root -w "/var/lib/tomcat8/build" --volume "$ROOTDIR:/var/lib/tomcat8/build" docker.alkante.com/alk-tomcat:8 ./mvnw install:install-file -DgroupId=org.geonetwork-opensource -DartifactId=geonetwork -Dversion=3.10.4-0 -Dpackaging=war -Dfile=./source/geonetwork-3.10.4-0.war clean package

docker run -u root -w "/var/lib/tomcat8/build" --volume "$ROOTDIR:/var/lib/tomcat8/build" docker.alkante.com/alk-tomcat:8 chown -R $(id -u):$(id -g) .
