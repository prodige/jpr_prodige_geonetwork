#!/bin/sh
set -e

# Source env file
SCRIPT=`readlink -f $0`
SCRIPTDIR=`dirname $SCRIPT`
SCRIPTNAME=`basename $SCRIPT`
. $SCRIPTDIR/env.sh

echo "  Start 99_clean.sh"

# Clean container gerenated
list="${DOCKER_NAME}_web ${DOCKER_NAME}_build_web"
for var in $list
do
  if [ "$(docker ps -aq -f name=${var})" ]; then
    echo "  Clean container ${var}"
    docker rm -f ${var}
  fi

done 

# Clean image gerenated
list="${DOCKER_NAME}_build_web"
for var in $list
do
  if [ "$(docker images -q -f reference=${var})" ]; then
    echo "  Clean images ${var}"
    docker rmi ${var}
  fi
done 

echo "  End 99_clean.sh"
