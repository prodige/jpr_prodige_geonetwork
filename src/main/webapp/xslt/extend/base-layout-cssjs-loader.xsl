<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:util="java:org.fao.geonet.util.XslUtil"
                exclude-result-prefixes="#all">
  <!-- Template to load CSS and Javascript -->

  <xsl:include href="base-variables.xsl"/>
  
  <xsl:template name="ux-css-load">
  <!-- ALKANTE EXTENSION -->
    <link href="{$uiResourcesPath}views/apps_extend/bootstrap-submenu-2.0.4/css/bootstrap-submenu.css" rel="stylesheet" media="screen" />
    <link href="{$uiResourcesPath}views/apps_extend/css/apps_extend.css{$minimizedParam}" rel="stylesheet" media="screen" />
  </xsl:template>


  <xsl:template name="ux-javascript-load">    
    <script type="text/javascript">
      var __PRODIGE_CATALOGUE_CONFIG_URL__ = '<xsl:value-of select="/root/gui/config/prodige/catalogue/url"/>';
      var __PRODIGE_BROADCAST_CONNECT_URL__ = [
      <xsl:for-each select="/root/gui/config/prodige/broadcast-connect/url">
        '<xsl:value-of select="text()" />'<xsl:if test="position() != last()">,</xsl:if>
      </xsl:for-each>
      ];
    </script>
    <!-- ALKANTE_EXTENSION [-->
    <script type="text/javascript" src="{$uiResourcesPath}lib/jquery.ext/jquery.cookie.js"></script>
    <script type="text/javascript" src="{$uiResourcesPath}views/apps_extend/bootstrap-submenu-2.0.4/js/bootstrap-submenu.js"></script>
    <script type="text/javascript" src="{$uiResourcesPath}views/apps_extend/util.js"></script>
    <script type="text/javascript" src="{$uiResourcesPath}views/apps_extend/administration_carto.js"></script>
    <script type="text/javascript" src="{$uiResourcesPath}views/apps_extend/actions.js"></script>
    <script type="text/javascript" src="{$uiResourcesPath}views/apps_extend/admin.js"></script>
    <script type="text/javascript" src="{$uiResourcesPath}views/apps_extend/panier.js"></script>
    <!-- ] ALKANTE_EXTENSION -->
  </xsl:template>
</xsl:stylesheet>
