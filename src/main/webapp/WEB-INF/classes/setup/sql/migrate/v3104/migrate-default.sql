UPDATE Settings SET value='3.10.4' WHERE name='system/platform/version';
UPDATE Settings SET value='0' WHERE name='system/platform/subVersion';

INSERT INTO Settings (name, value, datatype, position, internal) VALUES ('system/users/identicon', 'gravatar:mp', 0, 9110, 'n');

update public.settings_ui set configuration='{
            "langDetector": {
              "fromHtmlTag": false,
              "regexp": "^(?:/.+)?/.+/([a-z]{2,3})/.+",
              "default": "eng"
            },
            "nodeDetector": {
              "regexp": "^(?:/.+)?/(.+)/[a-z]{2,3}/.+",
              "default": "srv"
            },
            "serviceDetector": {
              "regexp": "^(?:/.+)?/.+/[a-z]{2,3}/(.+)",
              "default": "catalog.search"
            },
            "baseURLDetector": {
              "regexp": "^((?:/.+)?)+/.+/[a-z]{2,3}/.+",
              "default": "/geonetwork"
            },
            "mods": {
              "global": {
                "humanizeDates": true
              },
              "footer": {
                "enabled": true,
                "showSocialBarInFooter": true
              },
              "header": {
                "enabled": true,
                "languages": {
                  "eng": "en",
                  "dut": "nl",
                  "fre": "fr",
                  "ger": "ge",
                  "kor": "ko",
                  "spa": "es",
                  "cze": "cz",
                  "cat": "ca",
                  "fin": "fi",
                  "ice": "is",
                  "rus": "ru",
                  "chi": "zh"
                },
                "isLogoInHeader": false,
                "logoInHeaderPosition": "left",
                "fluidHeaderLayout": true,
                "showGNName": true,
                "isHeaderFixed": false
              },
              "cookieWarning": {
                "enabled": true,
                "cookieWarningMoreInfoLink": "",
                "cookieWarningRejectLink": ""
              },
              "home": {
                "enabled": true,
                "appUrl": "../../srv/{{lang}}/catalog.search#/home",
                "fluidLayout": true
              },
              "search": {
                "enabled": true,
                "appUrl": "../../srv/{{lang}}/catalog.search#/search",
                "hitsperpageValues": [
                  10,
                  50,
                  100
                ],
                "paginationInfo": {
                  "hitsPerPage": 20
                },
                "facetsSummaryType": "details",
                "defaultSearchString": "",
                "facetTabField": "",
                "facetConfig": [],
                "filters": {},
                "sortbyValues": [
                  {
                    "sortBy": "relevance",
                    "sortOrder": ""
                  },
                  {
                    "sortBy": "changeDate",
                    "sortOrder": ""
                  },
                  {
                    "sortBy": "title",
                    "sortOrder": "reverse"
                  },
                  {
                    "sortBy": "rating",
                    "sortOrder": ""
                  },
                  {
                    "sortBy": "popularity",
                    "sortOrder": ""
                  },
                  {
                    "sortBy": "denominatorDesc",
                    "sortOrder": ""
                  },
                  {
                    "sortBy": "denominatorAsc",
                    "sortOrder": "reverse"
                  }
                ],
                "sortBy": "relevance",
                "resultViewTpls": [
                  {
                    "tplUrl": "../../catalog/components/search/resultsview/partials/viewtemplates/grid.html",
                    "tooltip": "Grid",
                    "icon": "fa-th"
                  },
                  {
                    "tplUrl": "../../catalog/components/search/resultsview/partials/viewtemplates/list.html",
                    "tooltip": "List",
                    "icon": "fa-bars"
                  }
                ],
                "resultTemplate": "../../catalog/components/search/resultsview/partials/viewtemplates/grid.html",
                "formatter": {
                  "list": [
                    {
                      "label": "defaultView",
                      "url": ""
                    },
                    {
                      "label": "full",
                      "url": "/formatters/xsl-view?root=div&view=advanced"
                    }
                  ],
                  "defaultUrl": ""
                },
                "downloadFormatter": [
                  {
                    "label": "exportMEF",
                    "url": "/formatters/zip?withRelated=false",
                    "class": "fa-file-zip-o"
                  },
                  {
                    "label": "exportPDF",
                    "url": "/formatters/xsl-view?output=pdf&language=${lang}",
                    "class": "fa-file-pdf-o"
                  },
                  {
                    "label": "exportXML",
                    "url": "/formatters/xml",
                    "class": "fa-file-code-o"
                  }
                ],
                "grid": {
                  "related": [
                    "parent",
                    "children",
                    "services",
                    "datasets"
                  ]
                },
                "linkTypes": {
                  "links": [
                    "LINK",
                    "kml"
                  ],
                  "downloads": [
                    "DOWNLOAD"
                  ],
                  "layers": [
                    "OGC",
                    "ESRI:REST"
                  ],
                  "maps": [
                    "ows"
                  ]
                },
                "isFilterTagsDisplayedInSearch": true,
                "usersearches": {
                  "enabled": true,
                  "displayFeaturedSearchesPanel": true
                },
                "savedSelection": {
                  "enabled": true
                },
                "advancedSearchTemplate": "../../catalog/views/default/templates/advancedSearchForm/defaultAdvancedSearchForm.html"
              },
              "map": {
                "enabled": false,
                "appUrl": "../../srv/{{lang}}/catalog.search#/map",
                "externalViewer": {
                  "enabled": false,
                  "enabledViewAction": false,
                  "baseUrl": "http://www.example.com/viewer",
                  "urlTemplate": "http://www.example.com/viewer?url=${service.url}&type=${service.type}&layer=${service.title}&lang=${iso2lang}&title=${md.defaultTitle}",
                  "openNewWindow": false,
                  "valuesSeparator": ","
                },
                "is3DModeAllowed": true,
                "isSaveMapInCatalogAllowed": true,
                "isExportMapAsImageEnabled": true,
                "storage": "sessionStorage",
                "bingKey": "AnElW2Zqi4fI-9cYx1LHiQfokQ9GrNzcjOh_p_0hkO1yo78ba8zTLARcLBIf8H6D",
                "listOfServices": {
                  "wms": [],
                  "wmts": []
                },
                "projection": "EPSG:3857",
                "projectionList": [
                  {
                    "code": "EPSG:4326",
                    "label": "WGS84(EPSG:4326)"
                  },
                  {
                    "code": "EPSG:3857",
                    "label": "Googlemercator(EPSG:3857)"
                  }
                ],
                "switcherProjectionList": [
                  {
                    "code": "EPSG:3857",
                    "label": "Google mercator (EPSG:3857)"
                  }
                ],
                "disabledTools": {
                  "processes": false,
                  "addLayers": false,
                  "projectionSwitcher": false,
                  "layers": false,
                  "legend": false,
                  "filter": false,
                  "contexts": false,
                  "print": false,
                  "mInteraction": false,
                  "graticule": false,
                  "syncAllLayers": false,
                  "drawVector": false
                },
                "graticuleOgcService": {},
                "map-viewer": {
                  "context": "../../map/config-viewer.xml",
                  "extent": [
                    0,
                    0,
                    0,
                    0
                  ],
                  "layers": []
                },
                "map-search": {
                  "context": "../../map/config-viewer.xml",
                  "extent": [
                    0,
                    0,
                    0,
                    0
                  ],
                  "layers": []
                },
                "map-editor": {
                  "context": "",
                  "extent": [
                    0,
                    0,
                    0,
                    0
                  ],
                  "layers": []
                },
                "autoFitOnLayer": false,
                "map": "../../map/config-viewer.xml",
                "useOSM": true,
                "context": "",
                "layer": {
                  "url": "http://www2.demis.nl/mapserver/wms.asp?",
                  "layers": "Countries",
                  "version": "1.1.1"
                },
                "searchMapLayers": [],
                "viewerMapLayers": []
              },
              "geocoder": {
                "enabled": true,
                "appUrl": "https://secure.geonames.org/searchJSON"
              },
              "recordview": {
                "enabled": true,
                "isSocialbarEnabled": true
              },
              "editor": {
                "enabled": true,
                "appUrl": "../../srv/{{lang}}/catalog.edit",
                "isUserRecordsOnly": false,
                "minUserProfileToCreateTemplate": "",
                "isFilterTagsDisplayed": false,
                "fluidEditorLayout": true,
                "createPageTpl": "../../catalog/templates/editor/new-metadata-horizontal.html",
                "editorIndentType": ""
              },
              "admin": {
                "enabled": true,
                "appUrl": "../../srv/{{lang}}/admin.console"
              },
              "signin": {
                "enabled": true,
                "appUrl": "../../srv/{{lang}}/catalog.signin"
              },
              "signout": {
                "appUrl": "../../signout"
              },
              "page": {
                "enabled": true,
                "appUrl": "../../{{node}}/{{lang}}/catalog.search#/page"
              }
            }
          }';
