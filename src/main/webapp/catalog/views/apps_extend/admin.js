var SSO_ENABLED = true;
if ( typeof Ext == "undefined" ) Ext = {};
if ( typeof Ext.menu == "undefined" ) Ext.menu = {};
if ( typeof Ext.Msg == "undefined" ) Ext.Msg = {};
if ( typeof Ext.menu.Item == "undefined" ) Ext.menu.Item = {};


if ( $.cookie && $.cookie("__PRODIGE_CATALOGUE_CONFIG_URL__") ){
    __PRODIGE_CATALOGUE_CONFIG_URL__ = $.cookie("__PRODIGE_CATALOGUE_CONFIG_URL__");
}

if ( $.cookie && $.cookie("__PRODIGE_BROADCAST_CONNECT_URL__") ){
    __PRODIGE_BROADCAST_CONNECT_URL__ = $.cookie("__PRODIGE_BROADCAST_CONNECT_URL__").split(',');
}
//(function() {
if (module){


  (initProdigeCatalogue = function (){
    $.ajaxSetup({
      xhrFields: {
        withCredentials: true
      }
    });
    prodigeConfig = angular.extend({}, window.catalogue||{});
    var gn_prodige = document.createElement('div');
    gn_prodige.setAttribute('ng-controller', 'gn_prodige_controller');
   // gn_prodige.setAttribute('ng-init', 'callXhrAsynchronous()');
    document.body.appendChild(gn_prodige);


    module.controller('gn_prodige_controller', [
    '$rootScope', '$scope', '$controller', '$http', '$q'/*, '$templateCache'*/,
    function ($rootScope, $scope, $controller, $http, $q/*, $templateCache*/) {
      $rootScope.prodige = {
        isIdentified : function(){
          return $scope.$parent && $scope.$parent.authenticated;
        },
        loaded : false
      };
      __PRODIGE_BROADCAST_CONNECT_URL__ = __PRODIGE_BROADCAST_CONNECT_URL__ || [];
      if ( __PRODIGE_BROADCAST_CONNECT_URL__.indexOf(__PRODIGE_CATALOGUE_CONFIG_URL__)==-1 )
        __PRODIGE_BROADCAST_CONNECT_URL__ = [__PRODIGE_CATALOGUE_CONFIG_URL__].concat(__PRODIGE_BROADCAST_CONNECT_URL__);

          var urls = __PRODIGE_BROADCAST_CONNECT_URL__;
      var ln = urls.length;
      var attemptConnect = {};
      var G_I = 0;
      var onSiteConnect = function(json, casuser){

        var i = (SSO_ENABLED ? G_I : json.index);
        if ( i<ln && !attemptConnect[i] ){
          attemptConnect[i] = true;
          $.ajax({
            async: false,
            xhrFields: {
                 withCredentials: true
            },
            crossDomain: true,
            dataType: "jsonp",
            url : (SSO_ENABLED
             ? (urls[i])+'/prodige/connect?index='+i+'&sessionid='+json.sessionid
             : (urls[i])+'/prodige/connect?casuser='+casuser+'&index='+i+'&sessionid='+json.sessionid
            ),
            success : function(data){
              if ( urls[i]==__PRODIGE_CATALOGUE_CONFIG_URL__ ) $scope.callXhrAsynchronous();
              if (SSO_ENABLED) {
                G_I++;
              }
              onSiteConnect(data, casuser)
            },
            failure : function(){
              if ( urls[i]==__PRODIGE_CATALOGUE_CONFIG_URL__ ) $scope.callXhrAsynchronous();
              if (SSO_ENABLED) {
                G_I++;
              }
              onSiteConnect(data, casuser)
            }
          });
        }
      };
      var connectUser = function(casuser){
        if ( SSO_ENABLED || casuser ){
          var i = (SSO_ENABLED ? G_I : 0);
          if ( i<ln  && !attemptConnect[i] ){
            attemptConnect[i] = true;
            $.ajax({
              xhrFields: {
                withCredentials: true
              },
              async: false,
              crossDomain: true,
              dataType: "jsonp",
              url : (SSO_ENABLED
               ? (urls[i])+'/prodige/connect?index='+i
               : (urls[i])+'/prodige/connect?casuser='+casuser+'&index='+i
              ),
              success : function(data){
                if ( urls[i]==__PRODIGE_CATALOGUE_CONFIG_URL__ ) $scope.callXhrAsynchronous();
                if (SSO_ENABLED) {
                  G_I++;
                }
                onSiteConnect(data, casuser)
              },
              failure : function(){
                if ( urls[i]==__PRODIGE_CATALOGUE_CONFIG_URL__ ) $scope.callXhrAsynchronous();
                if (SSO_ENABLED) {
                  G_I++;
                }
                onSiteConnect(data, casuser)
              }
            });
          }
        }
      }
      /**
       * watch $parent.user to force a prodige auth connection and get the user config
       */
      $scope.$parent.$watch('user', function() {
        if( $scope.$parent.user ) {
          $scope.$parent.$watch('authenticated', function(){$scope.$parent.authenticated && connectUser($scope.$parent.user['username'])});
        } else {
          connectUser('vinternet');
        }
      });


      $scope.myXhr = function(){
        var deferred = $q.defer();
        $.ajax({
          xhrFields: {
            withCredentials: true
          },
          async: false,
          crossDomain: true,
          dataType: "jsonp",
          url : __PRODIGE_CATALOGUE_CONFIG_URL__+'/geosource/get_config',
          method: 'GET',
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
          success : function(data,status,headers,config){
              //resolve the promise
              deferred.resolve(data);
          },
        //if request is not successful
          failure : function(data,status,headers,config){
              //reject the promise
            console.log(status);
            console.log(data);
            console.log(headers);
            console.log(config);
            deferred.reject('ERROR');
          }
        });
        //return the promise
        return deferred.promise;
      };
      $scope.callXhrAsynchronous = function(){
        /**
         * disable 'Users and groups' button in admin.console
         */
        angular.element('a[href="#organization"]').addClass('disabled hidden');

        var myPromise = $scope.myXhr();
        // wait until the promise return resolve or eject
        //"then" has 2 functions (resolveFunction, rejectFunction)
        myPromise.then(function(data){
          angular.extend($rootScope.prodige, data);
          $rootScope.prodige.loaded = true;
          prodigeConfig = angular.extend({}, $rootScope.prodige);
        },
        function(reject){
        });
      };

    }]);

    /**
     * Chargement des fichiers de langue de l'extension Alkante
     */
    module.config(['$LOCALES', function($LOCALES) {
      $LOCALES.push('alk-extend');
    }]);

  })();

  (topToolbar = function (){
    module.directive('navbarHeader', function($compile, $rootScope){
      return {
        restrict: 'C',
        scope: false,
        link : function(scope, element){
          $rootScope.$watch('prodige.loaded', function(){
            if ( !$rootScope.prodige.loaded ) return;
            var div = document.createElement('div')
            div.setAttribute('ng-controller', "toolbar");
            div.setAttribute('data-ng-include', "'../../catalog/views/apps_extend/templates/top-toolbar.html'");
            $(element).parent().find('ul.navbar-right').parent().append($(div));
            $compile($(div))(scope);
          })
        }
      }
    });
    module.directive('wellMd', function($compile, $rootScope){
      return {
        restrict: 'C',
        scope: false,
        link : function(scope, element){
          var img = document.createElement('img');
          img.className = "big-logo";
          img.setAttribute('data-ng-src', '../../catalog/views/apps_extend/logos/{{info.site.siteId}}.png?{{info.site.lastUpdate}}');
          var div = document.createElement('h1');
          div.appendChild(img);
          $(element).addClass('prodige-home');
          $(element).prepend($(div))
          $compile($(div))(scope);
        }
      }
    });
    module.directive('userAccountExpirationMsg', function($compile, $rootScope){
      return {
        restrict: 'AE',
        scope: false,
        link: function(scope, element, attrs) {
          $rootScope.$watch('prodige.loaded', function(){
            if ( !$rootScope.prodige.loaded ) return;

            if( $rootScope.prodige.userDetails && $rootScope.prodige.userDetails.alert == true ) {
              var div = $('<i class="fa fa-warning text-warning" title="'+$rootScope.prodige.userDetails.message+'"></i>');
              $(element).append(div);
              $compile(div)(scope);
            }
          });
        }
      }
    });
  })();

  (helpModal = function(){
    module.controller( 'helpModal', [
      '$rootScope', '$scope', '$controller', '$http'
    , function($rootScope, $scope, $controller, $http){
        // move modal to body to avoid backdrop overlap
        $('#helpModal').appendTo('body');
        $scope.help = [];
        $.ajax({
          xhrFields: {
               withCredentials: true
          },
          crossDomain: true,
          method  : 'GET',
          url     : prodigeConfig.routes.catalogue.geosource_get_help,
          withCredentials: true,
          success : function(data) {
            $scope.help = data;
          }
        });
      }
    ]);
  })();

  (toolbar = function(){
    module.controller( 'toolbar', [
      '$rootScope', '$scope', '$controller', '$http'
    , function($rootScope, $scope, $controller, $http){

        $scope.initContactForm = function() {
          // broadcast to child scopes
          $rootScope.$broadcast('initContactForm');
        };

      }
    ]);
  })();

  (contactAdmin = function(){
    module.controller( 'formContactAdmin', [
      '$rootScope', '$scope', '$controller', '$http'
    , function($rootScope, $scope, $controller, $http){
        // move modal to body to avoid backdrop overlap
        $('#contactAdmin').appendTo('body');
        $scope.model = {
          usr_name       : $rootScope.prodige.isIdentified() ? $rootScope.prodige.usr_name : '',
          usr_firstname  : $rootScope.prodige.isIdentified() ? $rootScope.prodige.usr_firstname : '',
          usr_email      : $rootScope.prodige.isIdentified() ? $rootScope.prodige.usr_mail : ''
        };
        $scope.control = "";
        $scope.refreshCaptcha = function(){
          $scope.control = String(Math.round(Math.random()*1000000000));
          $('#captchaimg').get(0).src = prodigeConfig.URL+'/geosource/captcha' + "?rand="+$scope.control;
        };
        $scope.initForm = function(){
          $scope.model = {
            usr_name       : $rootScope.prodige.isIdentified() ? $rootScope.prodige.usr_name : '',
            usr_firstname  : $rootScope.prodige.isIdentified() ? $rootScope.prodige.usr_firstname : '',
            usr_email      : $rootScope.prodige.isIdentified() ? $rootScope.prodige.usr_mail : '',
            captcha : '',
            message: ''
          };
          $scope.refreshCaptcha();
        }
        // init form data when notified to (see toolbar controller)
        $rootScope.$on('initContactForm', function(e){
          $scope.initForm();
        });
        $scope.processForm = function(){
          if ( $scope.formContactAdmin.$valid ){
            $http({
              method  : 'POST',
              url     : prodigeConfig.routes.catalogue.geosource_contact_admin,
              data    : $.param($scope.model),  // pass in data as strings
              withCredentials: true,
              headers : { 'Content-Type': 'application/x-www-form-urlencoded' }  // set the headers so angular passing info as form data (not request payload)
            })
            .then(
              function(data) { // success
                //console.log('submit', data);
                // re-init form with default values
                $scope.initForm();
                // close popup
                angular.element('#contactAdmin').modal('hide');
                alert("Votre message a bien été envoyé à l'administrateur de la plate-forme");
              },
              function(data) { // error
                console.log('#contactAdmin', data);
              }
            )
          } else {
            console.log($scope.formContactAdmin.$error);
          }
        };
      }
    ]);
  })();

  function configureMetadata(md){
    if ( !md || md.get ) return;
    md.get = md.get || function(field){
      var result, temp;
      var traduction = {
        'isharvested' : 'isHarvested',
        'istemplate' : 'isTemplate',
        'Data' : 'title',
        'spatialRepresentationType' : 'spatialRepresentationType_text'
      };
      if ( typeof traduction[field]!="undefined" ){
        field = traduction[field];
      }
      try {
        switch(field){
          case "id" :
            temp = this['geonet:info'].id;
          break;
          case "id" : case "uuid" :
            temp = (this.getUuid ? this.getUuid() : this['geonet:info'].uuid);
          break;
          case "links" :
            temp = this['link'] || [];
            var res = [];
            temp.forEach(function(value){
                var tokens = value.split('|');
                res.push({
                  name: tokens[0],
                  title: tokens[1],
                  href: tokens[2],
                  protocol: tokens[3],
                  type: tokens[4]
                })
            })
            return res;
          break;
          case "serviceType" :
            var type = ((temp = this.type) instanceof Array ? (temp[0].value || temp[0]) : temp);
            if ( type=="map" ) return "invoke";
            if ( type=="service-invoke" ) return "invoke";
          default :
            temp = this[field];
          break;
        }
        result = (temp instanceof Array ? (temp[0].value || temp[0]) : temp);
        switch(field){
          case "type" :
            result = result.split('-')[0];
            switch(result){
              case "map" : result = "service"; break;
              case "service-invoke" : result = "service"; break;
            }
          break;
        }
      } catch(e){ }
      return result;
    }
    return md;
  };

  (panier = function(){
    panier_ajouter = null;
    module.controller( 'metadataCart', [
      '$rootScope', '$scope', '$controller', '$http'
    , function($rootScope, $scope, $controller, $http){
        // move modal to body to avoid backdrop overlap
        $('#metadataCart').appendTo('body');
        //$scope.$apply();
        var panier = JSON.parse(window.sessionStorage.getItem('PRODIGE.PANIER') || '{}');
        $scope.metadatas = panier.metadatas || [];
        $scope.indexes = panier.indexes || {};
        window.sessionStorage.setItem('PRODIGE.PANIER', JSON.stringify({metadatas : $scope.metadatas, indexes : $scope.indexes}));

          $('#metadataCart').on('shown.bs.modal', function(){
            if ( !$scope.metadatas ) return;
            $scope.metadatas.forEach(function(metadata){
            	if (metadata.can_storage===null && metadata.query_storage) {
            	    metadata.can_storage = true;
                  delete metadata.query_storage;
            	}
            });
            window.sessionStorage.setItem('PRODIGE.PANIER', JSON.stringify({metadatas : $scope.metadatas, indexes : $scope.indexes}));
            $scope.$apply();
          });
        window.panier_ajouter = $scope.panier_ajouter = function (metadata){
          if ( !metadata ) return false;
          configureMetadata(metadata);
          var type = metadata.get('type');

          switch(type){
            case "dataset":
            case "nonGeographicDataset":
              if ( metadata.can_storage===false ) return false;
              //nothing to do
            break;
            case "series":
              // search metadata children of the series to add them in basket
              var canAdd = true;
              $.ajax({
                async: false,
                url : '/geonetwork/srv/api/records/'+metadata.get("uuid")+'/related',
                data : {
                  type : "children"
                },
                dataType: "json",
                accepts: {
                    text: "application/json"
                },
                success : function(res) {
                    if (typeof res == "string") res = JSON.parse(res);
                    var metadatas = [];
                    if ( res!==null ){
                      if ( !(res.children instanceof Array) ){res.children = [res.children];}
                      metadatas = res.children;
                    }

                    var nbAdd = 0;
                    var metadatasByIds = {};
                    var ajaxMetadatas = [];
                    var harvested = [];
                    metadatas.forEach(function(metadataChild){
                      $.ajax({
                        async: false,
                        url : '/geonetwork/srv/fre/qi?_content_type=json&fast=index&uuid='+metadataChild.id,
                        dataType: "json",
                        accepts: {
                            text: "application/json"
                        },
                        success : function(res) {
                          if (typeof res == "string") res = JSON.parse(res);
                          metadata = res.metadata;

                          configureMetadata(metadata);
                          var type = metadata.get('type'), serviceType = metadata.get('serviceType');

                          if ( ["series", "dataset", "nonGeographicDataset"].indexOf(type)==-1 ) return;
                          var isHarvested = metadata.get('isHarvested');
                          if ( isHarvested=="y" ){
                            var tabLinks = metadata.get("links");
                            metadata.can_storage = tabLinks.length>0;
                            window.panier_ajouter(metadata) && nbAdd++;
                            return;
                          }


                          ajaxMetadatas.push({
                            ID : metadata.get('id'),
                            OBJET_TYPE : type,
                            OBJET_STYPE : (type == "service" && serviceType ? serviceType : "")
                          });
                          metadatasByIds[metadata.get('id')] = metadata;
                          }
                        });
                    });

                  if ( ajaxMetadatas.length>0 ) {
                    var traitements = ["TELECHARGEMENT", "NAVIGATION"];
                    $.ajax({
                      async : false,
                      crossDomain: true,
                      url : prodigeConfig.routes.catalogue.prodige_verify_rights_url+'_multiple',
                      data : {
                        metadatas : ajaxMetadatas,
                        TRAITEMENTS : traitements.join("|")
                      },
                      success : function(oRights) {
                        oRights = (typeof oRights=="string" ? JSON.parse(oRights) : oRights);
                        for (var id in oRights ){
                          var metadata = metadatasByIds[id];
                          metadata.can_storage = traitements.some(function(traitement){return !!oRights[id][traitement];});
                          if ( metadata.can_storage ){
                            nbAdd++;
                            window.panier_ajouter(metadata);
                          }
                        }
                      }
                    });
                  }
                  if ( nbAdd==0 ) {
                    canAdd = false;
                  }
                }
              });
              return canAdd;
             break;
             default : /*not add other types in basket*/ return false;
          }

          var serviceType = metadata.get('serviceType');
          var id = metadata.get("id");

          if ( typeof $scope.indexes[id] == "undefined" ){
            metadata.can_storage = (typeof metadata.can_storage=="undefined" ? null : metadata.can_storage);
            $scope.indexes[id] = $scope.metadatas.length;
            $scope.metadatas.push(metadata);
            window.sessionStorage.setItem('PRODIGE.PANIER', JSON.stringify({metadatas : $scope.metadatas, indexes : $scope.indexes}));
            //$scope.$apply();
          }
          $('#metadataCart').modal('show');

          if ( metadata.can_storage===null ){

            if ( metadata.get("isharvested")=="y" ){
              var tabLinks = metadata.get("links");
              metadata.can_storage = tabLinks.length>0;
              if ( !metadata.can_storage ) $scope.panier_retirer(metadata);
              $scope.$apply();
            }else{
            	$scope.metadatas[$scope.indexes[id]].query_storage = true;
              var traitements = ["TELECHARGEMENT", "NAVIGATION"];
              $.ajax({
                crossDomain: true,
                url : prodigeConfig.routes.catalogue.prodige_verify_rights_url,
                data : {
                  ID : id,
                  OBJET_TYPE : type,
                  OBJET_STYPE : (type == "service" && serviceType ? serviceType : ""),
                  TRAITEMENTS : traitements.join("|")
                },
                success : function(oRights) {
                	delete $scope.metadatas[$scope.indexes[id]].query_storage;
                  oRights = (typeof oRights=="string" ? JSON.parse(oRights) : oRights);
                  $scope.metadatas[$scope.indexes[id]].can_storage = traitements.some(function(traitement){return !!oRights[traitement];});
                  if ( !$scope.metadatas[$scope.indexes[id]].can_storage ) $scope.panier_retirer($scope.metadatas[$scope.indexes[id]]);
                  $scope.$apply();
                },
                failure : function(oRights) {
                  delete $scope.metadatas[$scope.indexes[id]].query_storage;
                	$scope.metadatas[$scope.indexes[id]].can_storage = false;
                }
              });
            }
          }
          return true;
        }; // panier_ajouter
        $scope.panier_retirer = function(metadata){
          if ( !metadata ) return;
          configureMetadata(metadata);
          var id = metadata.get('id');
          if ( typeof $scope.indexes[id] != "undefined" ){
              var index = $scope.indexes[id];
              var mds = [], ids = {};
              $scope.metadatas.forEach(function(md, mdIndex){
                if ( mdIndex==index ) return;
                ids[md['geonet:info'].id] = mds.length;
                mds.push(md);
              });
            window.sessionStorage.setItem('PRODIGE.PANIER', JSON.stringify({metadatas : ($scope.metadatas = mds), indexes : ($scope.indexes = ids)}));
          //  $scope.$apply();
          }
        }; // panier_retirer
        $scope.panier_vider = function(){
          $scope.metadatas = [];
          $scope.indexes = {};
          window.sessionStorage.setItem('PRODIGE.PANIER', JSON.stringify({metadatas : $scope.metadatas, indexes : $scope.indexes}));
          //$scope.$apply();
        };
        $scope.panier_covisualiser = function(metadata){
          window.panier_covisualiser($scope, prodigeConfig)
        };
        $scope.panier_downloadParametrage = function(metadata){
          window.panier_downloadParametrage($scope, prodigeConfig);
        };

      }
    ]);
  })();

  (prodigeActions = function(){
    module.directive('gnResultsContainer', function($compile, $rootScope, $http, $templateCache){
      return {
        restrict: 'A',
        link: function(scope, element, attrs, controller, transcludeFn) {
          // remplacer le template d'affichage de la liste de résultats dans le cas de la vue "Contribuer"
          if( $(element).closest('[data-ng-controller="GnEditorBoardSearchController"]').size() > 0 ) {
            $http
            .get('../../catalog/views/apps_extend/templates/editor.html', {cache: $templateCache})
            .success(function(tplContent){
              scope.prodigeIsContributeMenu = true;
              element.replaceWith($compile(tplContent)(scope));
            });
          }
        }
      };
    });
    module.directive('gnMdLinks', function($compile, $rootScope){
      return {
        //require: ['!searching'],
        restrict: 'C',
        //transclude : true,
        scope: false,
        link : function(scope, element){
          var div = document.createElement('gn-catalog-links')
          element.append($(div));

          $compile($(div))(scope);
          scope.prodigeMenu = $(div);
        }
      };
    });
    module.directive('gnMdActionsBtn', function($compile, $rootScope){
      return {
       // require: ['$root.prodige.loaded'],
        restrict: 'C',
        //transclude : true,
        scope: false,
        link : function(scope, element){
          var div = document.createElement('gn-catalog-links');
          $(element).before($(div));

          $compile($(div))(scope);
          scope.prodigeMenu = $(div);
          div.className = "gn-md-actions-btn gn-resultview pull-right";
        }
      };
    });
    module.directive('gnMdView', function($compile, $rootScope, $http){
      return {
        restrict: 'C',
        //transclude : true,
        //scope: false,
        link : function(scope, element){
          scope.$watch('gnMdViewObj.current.record', function(){
            if( scope.gnMdViewObj.current.record ) {
              //params: {logFile:"ConsultationMetadonnees",
              //         objectName : record.get("title").replace('"', '""')+"\",\""+record.get("id")}
              var record = scope.gnMdViewObj.current.record;
              $http({
                url : __PRODIGE_CATALOGUE_CONFIG_URL__+'/prodige/add_log',
                method: 'GET',
                params: {logFile:"ConsultationMetadonnees", objectName : (record.title||record.defaultTitle)+";"+record.getId()},
                withCredentials: true
              })
              .error(function(data,status,headers,config){
                console.log("Erreur d'ecriture du log 'ConsultationMetadonnees'");
              });
            }
          });
        }
      };
    });
    module.directive('gnSelectionActions', function($compile, $rootScope){
      return {
        transclue : true,
        scope : false,
        restrict: 'C',
        link : function(scope, element){
          var action = $(document.createElement('li'));
          action.attr('ng-show', "$root.prodige.loaded");
          action.html('<a ng-click="panier_ajouter_selection()"><i class="fa fa-star"></i>&nbsp;' +
                  '<span title="Choisir les éléments sélectionnés sur la page uniquement">Choisir ces données (sélections de la page)</span>' +
                  '</a>');
          $(element).find('.dropdown-menu').prepend(action);

          scope.panier_ajouter_selection = function(){
            var nbAdd = 0;
            var metadatas = scope.$parent.$parent.searchResults.records;
            var metadatasByIds = {};
            var ajaxMetadatas = [];
            metadatas.forEach(function(metadata){
              if ( !metadata["geonet:info"].selected ) return;
              configureMetadata(metadata);
              var type = metadata.get('type'), serviceType = metadata.get('serviceType');
              if ( ["series", "dataset", "nonGeographicDataset"].indexOf(type)==-1 ) return;
              var isHarvested = metadata.get('isHarvested');
              if ( isHarvested=="y" ){
                var tabLinks = metadata.get("links");
                metadata.can_storage = tabLinks.length>0;
                metadata.can_storage && window.panier_ajouter(metadata) && nbAdd++;
                return;
              }
              ajaxMetadatas.push({
                ID : metadata.get('id'),
              OBJET_TYPE : type,
              OBJET_STYPE : (type == "service" && serviceType ? serviceType : "")
              });
              metadatasByIds[metadata.get('id')] = metadata;
            });
            if ( ajaxMetadatas.length>=0 ) {
              var traitements = ["TELECHARGEMENT", "NAVIGATION"];
              $.ajax({
                  async : false,
                  crossDomain: true,
                  url : prodigeConfig.routes.catalogue.prodige_verify_rights_url+'_multiple',
                  data : {
                    metadatas : ajaxMetadatas,
                    TRAITEMENTS : traitements.join("|")
                  },
                  success : function(oRights) {
                    oRights = (typeof oRights=="string" ? JSON.parse(oRights) : oRights);

                    for (var id in oRights ){
                        var metadata = metadatasByIds[id];
                        metadata.can_storage = traitements.some(function(traitement){return !!oRights[id][traitement];});
                        if ( metadata.can_storage ){
                            window.panier_ajouter(metadata) && nbAdd++;
                        }
                    }
                  }
              });
            }
            if ( nbAdd==0 ) {
                Ext.Msg.alert('Ajout de la sélection dans le panier', 'Aucune métadonnée ne peut être ajoutée au panier.');
            }
          }
          $compile(action)(scope);
        }
      }
    });

    module.directive('gnCatalogLinks', function($rootScope){
      return {
        require: ['$root.prodige.loaded'],
        transclude : true,
        templateUrl : '../../catalog/views/apps_extend/templates/metadata-actions.html',
        controller : ['$scope', '$rootScope', '$controller', function($scope, $rootScope, $controller){

          // hide default geosource delete metadata actions
          angular.element('a.gn-md-delete-btn').hide(); //
          angular.element('.gn-selection-actions li>a[ng-click="mdService.deleteMd()"]').parent().hide(); // marche pas

          $scope.prodigeActions = [];
          $scope.setMetadataDecoration = function(){
            if ( !$scope.md ) return true;
            configureMetadata($scope.md);
            var legendTooltip;
            var legendConfig = {
              "vector"    : ["Série de données vecteur",  (legendTooltip="Une série de données géographiques est «une compilation identifiable de données géographiques» , une donnée géographique étant «toute donnée faisant directement ou indirectement référence à un lieu ou une zone géographique spécifique».")],
              //"grid"      : ["Série de données raster",    legendTooltip],
              "raster"    : ["Série de données raster",    legendTooltip],
              "nonGeographicDataset" : ["Série de données non géographique ou tabulaire"],
              "series"    : ["Ensemble de séries de données", "Un ensemble de séries de données géographiques est une compilation de séries de données géographiques partageant la même spécification de produit."],
              "map"       : ["Cartes"],
              "service"   : ["Services"],
              "chart"   : ["Graphes"]
            };

            var mdtype;
            var type = "unknown";
            switch (mdtype = $scope.md.get('type')){
              case "dataset" :
                switch ($scope.md.get('spatialRepresentationType_text')){
                  case "Vecteur" : type = "vector"; break;
                  case "Raster" : type = "raster"; break;
                  case "Tabulaire" : type = "nonGeographicDataset"; break;
                  default : type = 'nonGeographicDataset'; break;
                }
              break;
              case "service" :
                if($scope.md.get('keyword')=='infoChartAccessService'){
                  type = "chart";
                }else{
                  type = ($scope.md.get('serviceType')=="invoke" ? "map" : mdtype);
                }
              break;
              default :
                type = mdtype;
              break;
            }

            $scope.md && $scope.prodigeMenu.closest("[data-gn-fix-mdlinks]").addClass('metadata-'+type);

            //création d'un encart de légende
            /*if ( $scope.prodigeIsContributeMenu!=true && $('.legend-metadata').length==0 && $('[data-sortby-combo]').length!=0 ){
              var legend;
              legend = $('<div class="pull-right ng-isolate-scope"></div>');
              legend.insertAfter('[data-sortby-combo]');

              legend = $('<div class="btn-group legend-metadata">' +
                         '<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-tags"></i>&nbsp; Légende</span>&nbsp;<span class="caret"></span>' +
                         '</div>').appendTo(legend);
              legend = $('<ul class="dropdown-menu" role="menu"></ul>').appendTo(legend);

              $.each(legendConfig, function(cls, info){
                legend.append('<li'+(info[1] ? ' title="'+info[1]+'"' : '')+'><div class="icon metadata-'+cls+'"></div>&nbsp;'+info[0]+'<br/></li>');
              })

            }*/
            return true;
          }
          $scope.initMetadataActions = function(){
            var menu = $scope.prodigeMenu.find('.metadataActions');
            $(menu).trigger('beforeshow');
            menu.getEl = function(){return $(this).get(0)};
            menu.getId = function(){return $(this).get(0).id};
            menu.insert = function(index, el){
              if ( el instanceof Ext.menu.ItemPro ){
                $scope.prodigeActions.push(el);
                return $(this).append($(el.getEl()));
              }
              if ( el instanceof Ext.ActionPro ){
                $scope.prodigeActions.push(el);
                return $(this).append($(el.getEl()));
              }
              return $(this).append($(el))
            };
            window.setMetadataActions($rootScope, $scope, menu, $scope.md, false, prodigeConfig);
          }
        }]
      };
    });
  })();//end prodigeActions

  /*$scope.confirm = function(title, body, success, fn_scope){
    $scope.msg = {msg_type : "alert", msg_title : title, msg_body : body};
    $scope.msg.confirmFn = function(){
      if ( success )
        success.call(fn_scope||this);
      $scope.showMessage = false;
    };
    $scope.showMessage = true;
  };*/


  (extMessages = function (){
    module./*directive( 'extMessages', function($compile){return {
      restrict : 'A',
      scope : false,
      */
      controller( 'extMessages', /* :*/ [ '$scope', "$controller", "$timeout"
    , function(/*scope,element*/$scope, $controller, $timeout){
        // move modal to body to avoid backdrop overlap
        $('#extMessages').appendTo('body');
        //console.log(arguments);
        if ( typeof Ext == "undefined" ) Ext = {};
        if ( typeof Ext.Msg == "undefined" ) Ext.Msg = {};

        $scope.modaltype = "alert";
        $scope.title = "Title";
        $scope.body  = "Body";

        $scope.close = function(){
          $('#extMessages #autoclose').click();
        }

        $scope.info = function(title, body, success, fn_scope){
          $('#extMessages').zIndex( 10000 ).modal('show');
          //diffère l'assignation des variables pour prise en compte
          $timeout(function(){
            $scope.modaltype = "info";
            $scope.title = title;
            $scope.body  = body;
          }, 0);
        };



        $scope.alert = function(title, body, success, fn_scope){
          $('#extMessages').zIndex( 10000 ).modal('show');
          //diffère l'assignation des variables pour prise en compte
          $timeout(function(){
            $scope.modaltype = "alert";
            $scope.title = title;
            $scope.body  = body;
            $scope.confirmFn = function(btn){
              if ( success ) {
                success.apply(fn_scope||$scope, arguments);
              }
            };
          }, 0);

        };

        $scope.confirm = function(title, body, success, fn_scope){
          $('#extMessages').zIndex( 10000 ).modal('show');
          //diffère l'assignation des variables pour prise en compte
          $timeout(function(){
            $scope.modaltype = "confirm";
            $scope.title = title;
            $scope.body  = body;
            $scope.confirmFn = function(btn){
              if ( success ) {
                success.apply(fn_scope||$scope, arguments);
              }
            };
          }, 0);

        };

        Ext.Msg.info = function(title, body, success, fn_scope){
          $scope.info(title, body, success, fn_scope);
          return $scope.close;
        };
        Ext.Msg.alert = function(title, body, success, fn_scope){
          $scope.alert(title, body, success, fn_scope);
          return $scope;
        };
        Ext.Msg.confirm = function(title, body, success, fn_scope){
          $scope.confirm(title, body, success, fn_scope);
          return $scope;
        };
      }
    ]
    //}}
    );
  })();
}

