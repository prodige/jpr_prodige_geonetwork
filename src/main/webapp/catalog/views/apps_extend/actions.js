if ( typeof Ext == "undefined" ) Ext = {};
if ( typeof Ext.menu == "undefined" ) Ext.menu = {};
if ( typeof Ext.Msg == "undefined" ) Ext.Msg = {};
if ( typeof Ext.menu.Item == "undefined" ) Ext.menu.Item = {};
if ( typeof Ext.DomQuery == "undefined" ) Ext.DomQuery = {};

/**
 * define class Ext.menu.Item
 */
Ext.menu.ItemPro = function(config) {
  angular.extend(this, config);
};
angular.extend(Ext.menu.ItemPro.prototype, {
  items : [],
  getEl : function(){
    this.disabled = false;
    if ( this.el ) return this.el;
    var me = this;
    this.el = document.createElement('li');
    this.el.className = "dropdown-submenu";

    this.anchor = document.createElement('a');
    $(this.anchor).click(function(){me.handler && me.handler()});
    $(this.anchor).html(' '+this.text);
    this.el.appendChild(this.anchor);
    // iconCls
    $(this.anchor).prepend($('<i class="'+me.iconCls+'"></i>'));

    if (this.menu.items && this.menu.items.length) {
      this.submenu = document.createElement('ul');
      this.submenu.className = "dropdown-menu dropdown-menu-right";
      this.el.appendChild(this.submenu);
      this.menu.items.forEach(function(action, index) {
        //console.log(index, action.getEl());
        $(action.getEl()).appendTo(me.submenu)
      })
    }
    return this.el;
  },
  setText : function(text) {
    this.text = text;
    $(this.anchor).html(' '+this.text);
  },
  setDisabled : function(bDisabled) {
    this.disabled = bDisabled || false;
    if( this.disabled ) $(this.el).attr("disabled", bDisabled).addClass("disabled");
    else                $(this.el).removeAttr("disabled").removeClass("disabled")
    //$(this.el).toggleClass("disabled", this.disabled);
  },
  show : function(bDisabled) {
    this.setDisabled(bDisabled);
    $(this.el).toggle(true);
    if (this.actionMenu && this.refreshActionMenu) {
      this.actionMenu.showAt(this.actionMenu.getPosition());
    }
    if (this.actionMenu && this.actionMenu.nbActionPro) {
      this.actionMenu.nbActionPro--;
      if (this.actionMenu.nbActionPro == 0) {
        if (this.actionMenu.menu_loading) {
          this.actionMenu.menu_loading.style.display = "none";
        }
      }
    }
  },
  hide : function() {
    this.setDisabled(true);
    $(this.el).toggle(false);
    if (this.actionMenu && this.refreshActionMenu) {
      this.actionMenu.showAt(this.actionMenu.getPosition());
    }
    if (this.actionMenu && this.actionMenu.nbActionPro) {
      this.actionMenu.nbActionPro--;
      if (this.actionMenu.nbActionPro == 0) {
        if (this.actionMenu.menu_loading) {
          this.actionMenu.menu_loading.style.display = "none";
        }
      }
    }
  },
  /**
   * all instances must implement this function and call this.hide() or
   * this.show()
   */
  setVisibility : function(catalogue) {
    this.hide();
  },
  getTraitement : function(catalogue) {
    var tabTraitement = [];
    if (this.menu.items) {
      this.menu.items.forEach(function(action, index) {
        if (catalogue.isIdentified() && action.bConnect
            || !catalogue.isIdentified() && action.bSearch) {
          if (action.traitement) {
            if (action.traitement instanceof Array) {
              tabTraitement = tabTraitement.concat(action.traitement);
            } else {
              tabTraitement.push(action.traitement);
            }
          }
        }
      });
    }
    if (catalogue.isIdentified() && this.bConnect
        || !catalogue.isIdentified() && this.bSearch) {
      if (this.traitement) {
        if (this.traitement instanceof Array) {
          tabTraitement = tabTraitement.concat(this.traitement);
        } else {
          tabTraitement.push(this.traitement);
        }
      }
    }
    return tabTraitement.join("|");
  },
  setAttr : function(attr, val) {
    this[attr] = val;
    if (this.menu.items) {
      this.menu.items.forEach(function(action, index) {
            action[attr] = val;
            action.setHandler(action.handler, action);
          });
    }
  },
  setHandler : function(handler, scope){
    this.handler = function(){handler.call(scope||this)};
  }
});

/**
 * define class Ext.ActionPro
 */
Ext.ActionPro = function(config) {
  angular.extend(this, config);
};
angular.extend(Ext.ActionPro.prototype, {
  items : [],
  getEl : function(){
    this.disabled = false;
    if ( this.el ) return this.el;
    var me = this;
    this.el = document.createElement('li');
    this.anchor = document.createElement('a');
    $(this.anchor).click(function(){me.handler()});
    $(this.anchor).html(' '+this.text);
    this.el.appendChild(this.anchor);
    // iconCls
    $(this.anchor).prepend($('<i class="'+me.iconCls+'"></i>'));
    return this.el;
  },
  setText : function(text) {
    this.text = text;
    $(this.anchor).html(' '+this.text);
  },
  setDisabled : function(bDisabled) {
    this.disabled = bDisabled || false;
    if( this.disabled ) $(this.el).attr("disabled", bDisabled).addClass("disabled");
    else                $(this.el).removeAttr("disabled").removeClass("disabled")
    //$(this.el).toggleClass("disabled", this.disabled);
  },
  show : function(bDisabled) {
    this.setDisabled(bDisabled);
    $(this.el).toggle(true);
    if (this.actionMenu && this.refreshActionMenu) {
      this.actionMenu.showAt(this.actionMenu.getPosition());
    }
    this.resetToolTip();
    if (this.actionMenu && this.actionMenu.nbActionPro) {
      this.actionMenu.nbActionPro--;
      if (this.actionMenu.nbActionPro == 0) {
        if (this.actionMenu.menu_loading) {
          this.actionMenu.menu_loading.style.display = "none";
        }
      }
    }
  },
  hide : function() {
    this.setDisabled(true);
    $(this.el).toggle(false);
    if (this.actionMenu && this.refreshActionMenu) {
      this.actionMenu.showAt(this.actionMenu.getPosition());
    }
    if (this.actionMenu && this.actionMenu.nbActionPro) {
      this.actionMenu.nbActionPro--;
      if (this.actionMenu.nbActionPro == 0) {
        if (this.actionMenu.menu_loading) {
          this.actionMenu.menu_loading.style.display = "none";
        }
      }
    }
  },
  /**
   * all instances must implement this function and call this.hide() or
   * this.show()
   */
  setVisibility : function(catalogue) {
    this.hide();
  },
  getTraitement : function(catalogue) {
    if ((catalogue.isIdentified() && this.bConnect)
        || (!catalogue.isIdentified() && this.bSearch)) {
      if (this.traitement instanceof Array) {
        return this.traitement.join("|");
      } else {
        return this.traitement;
      }
    } else {
      return "";
    }
  },
  resetToolTip : function() {
    if (this.oMetadataSelect) {
      setToolTipToActionSelection(this);
    } else {
      setToolTipToAction(this);
    }
  },
  setAttr : function(attr, val) {
    this[attr] = val;
  },
  setHandler : function(handler, scope){
    this.handler = function(){handler.call(scope||this)};
  }
});

/**
 * set a toolTip to an action
 */
function setToolTipToAction(actionPro, toolTip) {
  actionPro.items.forEach(function(item) {
    if (item.oToolTip) {
      item.oToolTip.destroy();
    }
    _toolTip = (toolTip ? toolTip : (actionPro.toolTip
        ? actionPro.toolTip
        : ""));
    if (_toolTip) {
      item.oTolTip = new Ext.ToolTip({
            target : item.id,
            title : _toolTip
          });
    }
  });
}

/**
 * set a toolTip to an action on selection
 */
function setToolTipToActionSelection(actionPro, toolTip) {
  actionPro.items.forEach(function(item) {
    if (item.oToolTip) {
      item.oToolTip.destroy();
    }
    _toolTip = (toolTip ? toolTip : (actionPro.toolTip
        ? actionPro.toolTip
        : ""));
    if (_toolTip) {
      item.oToolTip = new Ext.ToolTip({
            target : item.id,
            title : _toolTip,
            anchor : "anchor"
          });
    }
  });
}

/**
 * set metadata actions
 */
function setMetadataActions($rootScope, $scope, actionMenu, record, addOnVisible, catalogue) {
  // add actions only if actionMenu exists

    // set toolTip before showing actions
//    actionMenu.on("beforeshow", function(menu) {
//      for (var i = 0; i < actionMenu.tabActionPro.length; i++) {
//        if (actionMenu.tabActionPro[i].resetToolTip) {
//          actionMenu.tabActionPro[i].resetToolTip();
//        }
//      }
//      // set toolTip to existing actions
//      if (actionMenu.viewAction) {
//        setToolTipToAction(actionMenu.viewAction,
//            "Consulter la fiche de métadonnée");
//      }
//      if (actionMenu.viewXMLAction) {
//        setToolTipToAction(actionMenu.viewXMLAction,
//            "Exporter la fiche de métadonnée au format XML");
//      }
//      if (actionMenu.viewXMLAction) {
//        setToolTipToAction(actionMenu.printAction,
//            "Exporter la fiche de métadonnée au format PDF");
//      }
//      if (actionMenu.getMEFAction) {
//        setToolTipToAction(actionMenu.getMEFAction,
//            "Exporter la fiche de métadonnée au format ZIP");
//      }
//    });

    // show loading

    if ($scope.metadata_uuid==record.get("uuid"))  return;

    $(actionMenu).html('')
    // add loading
    $scope.getting_rights = true;
    $scope.metadata_uuid = record.get("uuid");
    actionMenu.tabActionPro = [];
    var index = 0;
    /**PRODIGE40 : REMOVE
    // add action "Editer"
    index++;
    actionMenu.editNewAction = getEditNewAction(catalogue);
    actionMenu.insert(index, actionMenu.editNewAction);
    actionMenu.tabActionPro.push(actionMenu.editNewAction);*/
    // add action "Editer le catalogue d'attributs"
    /*index++;
    actionMenu.catalogueProAction = getCatalogueAction(catalogue);
    actionMenu.insert(index, actionMenu.catalogueProAction);
    actionMenu.tabActionPro.push(actionMenu.catalogueProAction);*/
    // add action "Importer les données"
    /*index++;
    actionMenu.insert(index, new Ext.ActionPro({
        text : "DEVELOPPEMENT : Ext Alert",
        handler : function(){ Ext.Msg.alert('TODO DEVELOPPEMENT : Ext Alert', "contenu<br>Sur plusieurs lignes <br>Fin.", function(){alert('ok')}) },
        setVisibility : function(){return true;}
      })
    );
    index++;
    actionMenu.insert(index, new Ext.ActionPro({
        text : "DEVELOPPEMENT : Ext Confirm",
        handler : function(){ Ext.Msg.confirm('TODO DEVELOPPEMENT : Ext Confirm', "contenu<br>Sur plusieurs lignes <br>Fin.", function(){alert('ok')}) },
        setVisibility : function(){return true;}
      })
    );*/
    // add action "Synchronisation des données SIG"
    actionMenu.synchronisationAction = getHarvestedSynchronizationAction(catalogue);
    actionMenu.insert(index, actionMenu.synchronisationAction);
    actionMenu.tabActionPro.push(actionMenu.synchronisationAction);
    // aadd action "Importer les données"
    index++;
    actionMenu.importDataAction = getImportDataAction(catalogue);
    actionMenu.insert(index, actionMenu.importDataAction);
    actionMenu.tabActionPro.push(actionMenu.importDataAction);
    // add action "Créer une donnée"
    index++;
    actionMenu.createStructureAction = getCreateStructureDataAction(catalogue);
    actionMenu.insert(index, actionMenu.createStructureAction);
    actionMenu.tabActionPro.push(actionMenu.createStructureAction);
    // add action "Dupliquer"
    /*index++;
    actionMenu.duplicateProAction = getDuplicateProAction(catalogue);
    actionMenu.insert(index, actionMenu.duplicateProAction);
    actionMenu.tabActionPro.push(actionMenu.duplicateProAction);*/
    // add action "Administrer la jointure"
    index++;
    actionMenu.adminJoinAction = getAdminJoinAction(catalogue);
    actionMenu.insert(index, actionMenu.adminJoinAction);
    actionMenu.tabActionPro.push(actionMenu.adminJoinAction);
    // add action "Paramétrer la carte"
    index++;
    actionMenu.paramCarteAction = getParamCarteAction(catalogue);
    actionMenu.insert(index, actionMenu.paramCarteAction);
    actionMenu.tabActionPro.push(actionMenu.paramCarteAction);
    index++;
    
    if(catalogue.PRO_IS_RAWGRAPH_ACTIF=="on"){
      actionMenu.paramChartAction = getParamChartAction(catalogue);
      actionMenu.insert(index, actionMenu.paramChartAction);
      actionMenu.tabActionPro.push(actionMenu.paramChartAction);
      index++;
    }
    
    // add action "Renommer la carte"
    /*index++;
    actionMenu.renameCarteAction = getRenameCarteAction(catalogue);
    actionMenu.insert(index, actionMenu.renameCarteAction);
    actionMenu.tabActionPro.push(actionMenu.renameCarteAction);*/
    // add action "Créer une métadonnée enfant"
    /*index++;
    actionMenu.createChildAction = getCreateChildAction(catalogue);
    actionMenu.insert(index, actionMenu.createChildAction);
    actionMenu.tabActionPro.push(actionMenu.createChildAction);*/
    // add action "Représentation par défaut"
    
    actionMenu.representationActions = getRepresentationActions(catalogue);
    actionMenu.insert(index, actionMenu.representationActions);
    actionMenu.tabActionPro.push(actionMenu.representationActions);
	
	  index++;
    actionMenu.qualiteActions = getQualiteActions(catalogue);
    actionMenu.insert(index, actionMenu.qualiteActions);
    actionMenu.tabActionPro.push(actionMenu.qualiteActions);
	
    // add sub-menu "Gérer l'accès public"
    index++;
    actionMenu.publicAccessActions = getPublicAccessActions(catalogue);
    actionMenu.insert(index, actionMenu.publicAccessActions);
    actionMenu.tabActionPro.push(actionMenu.publicAccessActions);
    // add sub-menu "Modifier le statut"
    /*index++;
    actionMenu.updateStatutActions = getUpdateStatutActions(catalogue);
    actionMenu.insert(index, actionMenu.updateStatutActions);
    actionMenu.tabActionPro.push(actionMenu.updateStatutActions);*/
    // add action "Supprimer"
    // add action "Ajouter au panier"
    index += 5;
    actionMenu.addPanierAction = getAddToPanierAction(catalogue);
    actionMenu.insert(index, actionMenu.addPanierAction);
    actionMenu.tabActionPro.push(actionMenu.addPanierAction);
    // add action "Visualiser"
	
    index++;
    actionMenu.visualiserAction = getVisualiserAction(catalogue);
    actionMenu.insert(index, actionMenu.visualiserAction);
    actionMenu.tabActionPro.push(actionMenu.visualiserAction);
	
	  index++;
    if(catalogue.PRO_IS_RAWGRAPH_ACTIF=="on"){
      actionMenu.visualiserChartAction = getVisualiserChartAction(catalogue);
      actionMenu.insert(index, actionMenu.visualiserChartAction);
      actionMenu.tabActionPro.push(actionMenu.visualiserChartAction);
    }
	
	
    // add action "Export PDF complet"
    index += 5;
    actionMenu.exportPDFCompletAction = getExportPDFCompletAction(catalogue);
    actionMenu.insert(index, actionMenu.exportPDFCompletAction);
    actionMenu.tabActionPro.push(actionMenu.exportPDFCompletAction);

    index++;
    actionMenu.deleteProAction = getDeleteProAction(catalogue);
    actionMenu.insert(index, actionMenu.deleteProAction);
    actionMenu.tabActionPro.push(actionMenu.deleteProAction);

  // set visibility on actions
  if (actionMenu
      && (!addOnVisible || addOnVisible
          && actionMenu.getEl().dom.style.top.substr(0, 1) != "-")) {
    if (addOnVisible) {
      actionMenu.showAt(actionMenu.getPosition());
    }
    // disable action "Editer"
    if (actionMenu.editAction) {
      actionMenu.editAction.hide();
      actionMenu.editAction.setDisabled(true);
    }
    if (actionMenu.extEditorAction) {
      actionMenu.extEditorAction.hide();
      actionMenu.extEditorAction.setDisabled(true);
    }
    if (actionMenu.angularEditorAction) {
      actionMenu.angularEditorAction.hide();
      actionMenu.angularEditorAction.setDisabled(true);
    }
    if (actionMenu.editNewAction) {
      actionMenu.editNewAction.hide();
      actionMenu.editNewAction.setDisabled(true);
    }
    // hide default action "Supprimer"
    if (actionMenu.deleteAction) {
      actionMenu.deleteAction.hide();
      actionMenu.deleteAction.setDisabled(true);
    }
    // hide action "Autres actions"
    if (actionMenu.otherActions) {
      actionMenu.otherActions.hide();
      actionMenu.otherActions.setDisabled(true);
    }
    if (catalogue.isIdentified()) {
      // hide action "Zoomer vers"
      if (actionMenu.zoomToAction) {
        actionMenu.zoomToAction.hide();
        actionMenu.zoomToAction.setDisabled(true);
      }
    }
    var tabTraitement = [];

    // init all actions visibility
    for (var i = 0; i < actionMenu.tabActionPro.length; i++) {
      actionMenu.tabActionPro[i].setAttr("actionMenu", actionMenu);
      actionMenu.tabActionPro[i].setAttr("record", record);
      actionMenu.tabActionPro[i].setAttr("refreshActionMenu", addOnVisible);
      actionMenu.tabActionPro[i].setVisibility(catalogue, true);
      var traitements = actionMenu.tabActionPro[i].getTraitement(catalogue);
      if ( !traitements ) traitements = [];
      if ( !(traitements instanceof Array) ) traitements = traitements.split("|");
      traitements.forEach(function(traitement){
        if ( traitement && tabTraitement.indexOf(traitement)==-1 )
          tabTraitement.push(traitement);
      })
    }
    // get user rights and show actions

    /**
     * fonction de verification des droits sur les actions du menu
     */
    var verifyRights = function() {
      if ( $scope.getting_rights ){
        $.ajax({
          //crossDomain: true,
          url : catalogue.routes.catalogue.prodige_verify_rights_url,
          data : {
            ID : record.get("id"),
            OBJET_TYPE : record.get('type'),
            OBJET_STYPE : ( in_array("chart", record.type) ? "chart" : 
			    (record.get('type') == "service"
                && record.get('serviceType') && record.get("isharvested") == "n"
                ? record.get('serviceType')
                : "")),
            TRAITEMENTS : tabTraitement.join("|")
          },
          success : function(oResUserRights) {
            $scope.getting_rights = true /*false*/;
            $scope.$digest()

            var oRights = (typeof oResUserRights=="string" ? eval("(" + oResUserRights + ")") : oResUserRights);
            if (actionMenu.editNewAction) {
              if (typeof oRights["CMS"] != 'undefined' && oRights["CMS"]) {
                actionMenu.editNewAction.show();
                actionMenu.editNewAction.setDisabled(false);
              } else {
                actionMenu.editNewAction.hide();
                actionMenu.editNewAction.setDisabled(true);
              }
            }
            actionMenu.nbActionPro = actionMenu.tabActionPro.length;
  //alert("TODO apps_extend/actions.js/setMetadataActions::ajax => restituer l'action setVisibility" );
            for (var i = 0; i < actionMenu.tabActionPro.length; i++) {
              actionMenu.tabActionPro[i].setAttr("oRights", oRights);

              RESTITUER : actionMenu.tabActionPro[i].setVisibility(catalogue);

  //            /*TO DELETE {*/
  //            if ( actionMenu.tabActionPro[i].action_id == 'catalogue' ){
  //              actionMenu.tabActionPro[i].setVisibility(catalogue);
  //            } else {
  //              actionMenu.tabActionPro[i].show();
  //              actionMenu.tabActionPro[i].setDisabled(false);
  //            }
  //            /*}TO DELETE*/
            }
            // hide default action "Supprimer"
            if (actionMenu.deleteAction) {
              actionMenu.deleteAction.hide();
              actionMenu.deleteAction.setDisabled(true);
            }
            // hide default action "Supprimer"
            if (actionMenu.angularEditorAction) {
              actionMenu.angularEditorAction.hide();
              actionMenu.angularEditorAction.setDisabled(true);
            }
            // hide action "Autres actions"
            if (actionMenu.otherActions) {
              actionMenu.otherActions.hide();
              actionMenu.otherActions.setDisabled(true);
            }

          },
          failure : function() {
            $scope.getting_rights = true /*false*/;
            console.log("failure");
          }
        });
      }
    } // end verifyRights
    $(actionMenu).on('beforeshow', verifyRights);
    $(actionMenu).trigger('beforeshow');

  }
}

/**
 * set metadata actions on selection
 */
function setSelectionMetadataActions(actionMenu, topToolBar, catalogue) {
  if (actionMenu) {

    if (!actionMenu.bProSelectionActionsAdded) {

      actionMenu.on("beforeshow", function(menu) {
        if (menu.tabSelectionActionPro) {
          menu.nbActionPro = menu.tabSelectionActionPro.length;
        }
        // show loading
        var menu_selection_loading = document
            .getElementById('menu_selection_loading_' + menu.getId());
        if (menu_selection_loading) {
          menu_selection_loading.style.display = "";
        } else {
          // add loading
          if (menu.getEl()) {
            var div = document.createElement('div');
            div.setAttribute('id', 'menu_selection_loading_' + menu.getId());
            div
                .setAttribute(
                    'style',
                    'position:absolute;top:0;width:100%;height:100%;background-color:rgba(255, 255, 255, 0.5)');
            var img = document.createElement('img');
            img.setAttribute('src', catalogue.URL + "/images/spinner.gif");
            img.setAttribute('style', 'display:block;margin:0 auto');
            div.appendChild(img);
            menu.getEl().dom.appendChild(div);
            menu.menu_loading = div;
          }
        }
        $.ajax({
              crossDomain: true,
              url : urlParent + "Services/selectMetadata.php",
              data : {
                action : "get",
                query : catalogue.services.rootUrl
                    + catalogue.metadataStore.service
                    + "?"
                    + GeoNetwork.util.SearchTools.buildQueryFromForm(Ext
                            .getCmp("searchForm"), catalogue.startRecord,
                        GeoNetwork.util.SearchTools.sortBy,
                        catalogue.metadataStore.fast)
              },
              success : function(oResMetadataSelect) {
                var oMetadataSelect = eval("("
                    + oResMetadataSelect + ")");
                this.tabSelectionActionPro.forEach(function(action) {
                  action.setAttr("oMetadataSelect", oMetadataSelect);
                  action.setVisibility(catalogue);
                });
              },
              scope : this
            });
        // set toolTip to existing actions on selection
        setToolTipToActionSelection(topToolBar.selectionActions[0],
            "Exporter la sélection au format ZIP");
        setToolTipToActionSelection(topToolBar.selectionActions[1],
            "Exporter la sélection au format CSV");
        setToolTipToActionSelection(topToolBar.selectionActions[2],
            "Exporter la sélection au format PDF");
        setToolTipToActionSelection(topToolBar.selectionActions[6],
            "Affecter une (des) catégorie(s)");
      });

      actionMenu.tabSelectionActionPro = [];
      var index = 1;
      // add action "Ajouter au panier"
      actionMenu.addSelectionToPanierActionPro = getAddToPanierAction(catalogue);
      actionMenu.insert(index, actionMenu.addSelectionToPanierActionPro);
      actionMenu.tabSelectionActionPro.push(actionMenu.addSelectionToPanierActionPro);

      // add action "Modifier le statut"
      actionMenu.addSelectionUpdateStatutActionsPro = getUpdateStatutActions(catalogue);
      actionMenu.insert(index, actionMenu.addSelectionUpdateStatutActionsPro);
      actionMenu.tabSelectionActionPro.push(actionMenu.addSelectionUpdateStatutActionsPro);

      // add action "Supprimer"
      index++;
      actionMenu.addSelectionDeleteActionPro = getDeleteProAction(catalogue);
      actionMenu.insert(index, actionMenu.addSelectionDeleteActionPro);
      actionMenu.tabSelectionActionPro.push(actionMenu.addSelectionDeleteActionPro);

      // add "Categories"
      /*index++
      actionMenu.addSelectionCategoriesActionPro = getCategoriesProAction(catalogue, topToolBar);
      actionMenu.insert(index, actionMenu.addSelectionCategoriesActionPro);
      actionMenu.tabSelectionActionPro.push(actionMenu.addSelectionCategoriesActionPro);
      */
      // attach actionMenu to action => useful to show/hide loading menu
      for (var i = 0; i < actionMenu.tabSelectionActionPro.length; i++) {
        actionMenu.tabSelectionActionPro[i].setAttr("actionMenu", actionMenu);
      }

   //   actionMenu.fireEvent("beforeshow", actionMenu);

      actionMenu.bProSelectionActionsAdded = true;
    }
  }
}

/**
 * return action "Importer les données"
 */
function getImportDataAction(catalogue) {
  return new Ext.ActionPro({
        text : "Importer les données...",
        iconCls : "fa fa-fw fa-upload",
        traitement : "CMS",
        bConnect : true,
        bSearch : false,
        handler : function() {
          if( this.disabled ) return;
          import_data(catalogue, this.record.get("uuid"));
        },
        setVisibility : function(catalogue, bInit) {
          if (catalogue.isIdentified() && this.bConnect
              || !catalogue.isIdentified() && this.bSearch) {
            if (bInit) {
              this.hide();
              return;
            }
            var sdom_isset = (this.oRights.sdom_dispose_metadata || false);
            switch (this.record.get('type') ) {
              case "series" :
                switch (this.oRights["couche_type"]) {
                  case "1" : // type "jointure"
                    this.show();
                  break;
                  default : // type "vector"
                    this.hide();
                  break;
                }
              case "dataset" :
                switch (this.oRights["couche_type"]) {
                  case "-4" : // type "jointure"
                    //this.hide();
                  //break;
                  default : // type "données", "majic", "table"
                    var enable = this.oRights[this.traitement];
                    //verify rights to edit metadata (creation mode for non harvested data)
                    /*if(!enable && this.record.get("isharvested")=="n"){
                      enable = Boolean( this.record["geonet:info"].edit ? this.record["geonet:info"].edit : false ) ;
                    }*/
                    this.show( !(sdom_isset && enable));
                  break;
                }
              break;
              case "nonGeographicDataset" :
                this.show( !(sdom_isset && this.oRights[this.traitement]));
              break;
              default :
                this.hide();
              break;
            }
          } else {
            this.hide();
          }
        },
        getTraitement : function(catalogue) {
          if (catalogue.isIdentified() && this.bConnect
              || !catalogue.isIdentified() && this.bSearch) {
            return this.traitement;
          } else {
            return "";
          }
        }
      });
}

/**
 * return action "Créer la Structure des données"
 */
function getCreateStructureDataAction(catalogue) {
  return new Ext.ActionPro({
        text : "Créer la structure de données",
        iconCls : "fa fa-fw fa-edit",
        traitement : "CMS",
        bConnect : true,
        bSearch : false,
        handler : function() {
          if( this.disabled ) return;
          administration_carto_creerStructure(catalogue, this.record.get('id'),
              this.oRights["layer_table"]||null, window.location.href,
              this.oRights["color_theme_id"], this.oRights["server_url"]);
        },
        setVisibility : function(catalogue, bInit) {
          if (catalogue.isIdentified() && this.bConnect
              || !catalogue.isIdentified() && this.bSearch) {
            if (bInit) {
              this.hide();
              return;
            }
            switch (this.record.get('type') ) {
              case "dataset" :
                switch (this.oRights["couche_type"]) {
                  case undefined :
                    if (this.oRights["CMS"] && this.oRights["PRO_EDITION"]){
                      var enable = this.oRights[this.traitement];
                      //verify rights to edit metadata (creation mode for non harvested data)
                      if(!enable && this.record.get("isharvested")=="n"){
                        enable = this.record["geonet:info"].edit ? this.record["geonet:info"].edit :false ;
                      }
                      this.show(!enable);
                    }else{
                      this.hide();
                    }
                  break;
				  case "" : // type "donnee"
                  case "1" : // type "donnee"
                    this.hide();
                  break;
                  default : // type "données", "majic", "table"
                    this.hide();
                  break;
                }
              break;
              default :
                this.hide();
              break;
            }
          } else {
            this.hide();
          }
        },
        getTraitement : function(catalogue) {
          if (catalogue.isIdentified() && this.bConnect
              || !catalogue.isIdentified() && this.bSearch) {
            return this.traitement;
          } else {
            return "";
          }
        }
      });
}

/**
 * return action "Affecter une(des) catégorie(s)
 */
/*
function getCategoriesProAction(catalogue, toptoolbar) {
  return new Ext.ActionPro({
    text : "Affecter une(des) catégorie(s)",
    iconCls : "",
    traitement : ["CMS"],
    bConnect : true,
    bSearch : false,
    handler : function() {
      if( $(this.el).prop('disabled') ) return;
      if (this.oMetadataSelect) {
        var tabMetadataDisallowedName = [];
        var tabMetadataDisallowed = [];
        var nbMetadataSelect = this.oMetadataSelect.length;
        this.oMetadataSelect.forEach(function(metadata) {
          $.ajax({
            //crossDomain: true,
            scope : this,
            url : catalogue.routes.catalogue.prodige_verify_rights_url,
            data : {
              ID : metadata.id,
              OBJET_TYPE : metadata.type,
              OBJET_STYPE : (metadata.type == "service" && metadata.serviceType
                  ? metadata.serviceType
                  : ""),
              TRAITEMENTS : this.traitement.join("|")
            },
            success : function(oResUserRights) {
              var oRights = (typeof oResUserRights=="string" ? eval("(" + oResUserRights + ")") : oResUserRights);
              if (!oRights[this.traitement[0]]) {
                tabMetadataDisallowed.push(metadata["uuid"]);
                tabMetadataDisallowedName.push(metadata["title"]);
              }
              nbMetadataSelect--;
              if (nbMetadataSelect == 0) {
                // On deselectionne les metadonnees dont les droits sont
                // insuffisants
                catalogue.metadataSelect('remove', tabMetadataDisallowed);
                // On affiche la fenetre des categories
                catalogue.massiveOp('Categories', function() {
                  var modalWindow = Ext.getCmp("modalWindow");
                  modalWindow.on("destroy", function() {
                    catalogue.metadataSelect('add', tabMetadataDisallowed);
                    var tabMetadataDisallowedLength = tabMetadataDisallowedName.length;
                    tabMetadataDisallowedName = tabMetadataDisallowedName
                        .join(", ");
                    if (tabMetadataDisallowedLength > 0) {
                      Ext.Msg.alert("Droits insuffisants",
                          (tabMetadataDisallowedLength == 1
                              ? "La couche "
                              : "Les couches ")
                              + tabMetadataDisallowedName
                              + (tabMetadataDisallowedLength == 1
                                  ? " n'a pu être mise "
                                  : " n'ont pu être mises ")
                              + "à jour. Droits insuffisants.");
                    }
                  });
                });
              }
            }
          });
        }, this);
      }
    },
    setVisibility : function(catalogue, bInit) {
      if ((catalogue.isIdentified() && this.bConnect) || (!catalogue.isIdentified() && this.bSearch)) {
        this.show(bInit);
        this.mode = "";
        if (this.oMetadataSelect) {
          if (this.oMetadataSelect.length == 0) {
            this.show(true);
          } else {
            this.show();
          }
        }
        if (this.oRights) {
          var type = this.record.get('type') ;
          var sub_type = (this.record.get('serviceType') || this.oRights["couche_type"]);
          this.mode = getMode(type, sub_type);
        }
      } else {
        this.hide();
      }
    }
  });
}
*/
/**
 * return action "Supprimer"
 */
function getDeleteProAction(catalogue) {
  //var doDelete = function(catalogue, uuid, type) {

    /*switch (type) {
      case "dataset" :
        administration_carto_delCouche(oRights["pk_data"], oRights["fmeta_id"],
            oRights["layer_table"], oRights["couche_type"],
            oRights["server_url"], title, onSuccess, tabParams);
      break;
      case "nonGeographicDataset" :
        administration_carto_delCoucheMajic(oRights["pk_data"],
            oRights["fmeta_id"], title, onSuccess, tabParams);
      break;
      case "series" :
        confirm_administration_carto_delDataConfirmed(oRights["pk_data"],
            oRights["fmeta_id"], 0, title, onSuccess, tabParams);
      break;
      case "service" :
        if (typeof oRights["pk_data"] != 'undefined'
            && typeof oRights["pk_stockage_carte"] != 'undefined') {
          administration_carto_delMap(oRights["pk_data"],
              oRights["pk_stockage_carte"], oRights["fmeta_id"], title,
              onSuccess, tabParams);
        } else {
          administration_carto_delService(oRights["fmeta_id"], title,
              onSuccess, tabParams);
        }
      break;
    }*/
  //}
  function doSelectionDelete(type, tabMetadataDelete) {
    if (tabMetadataDelete.length == 0) {
      Ext.Msg
          .alert(
              "Suppression de " + (type == "service" ? "carte" : "couche"),
              "Vous n'avez pas les droits suffisants pour supprimer les métadonnées sélectionnées.");
    } else {
      var doDeleteNext = function(doDelete, tabMetadataDelete) {
        var metadataDelete = tabMetadataDelete.pop();
        if (metadataDelete) {
          doDelete(metadataDelete.metadata.type, metadataDelete.metadata.title,
              metadataDelete.oRights, doDeleteNext, [doDelete, tabMetadataDelete]);
        } else {
          search();
        }
      };
      doDeleteNext(doDelete, tabMetadataDelete);
    }
  }

  return new Ext.ActionPro({
        text : "Supprimer",
        iconCls : "fa fa-fw fa-trash-o",
        traitement : "CMS",
        bConnect : true,
        bSearch : false,
        handler : function() {
          if( this.disabled ) return;
          if (this.oMetadataSelect) {
            var nbDel = this.oMetadataSelect.length;
            var tabMetadataDelete = [];
            this.oMetadataSelect.forEach(function(metadata) {
                  $.ajax({
                        //crossDomain: true,
                        scope : this,
                        url : catalogue.routes.catalogue.prodige_verify_rights_url,
                        data : {
                          ID : metadata.id,
                          OBJET_TYPE : metadata.type,
                          OBJET_STYPE : (metadata.type == "service"
                              && metadata.serviceType
                              ? metadata.serviceType
                              : ""),
                          TRAITEMENTS : this.traitement
                        },
                        success : function(oResUserRights) {
                          var enable = Boolean( this.record["geonet:info"].edit ? this.record["geonet:info"].edit : false ) ;
                          var oRights = (typeof oResUserRights=="string" ? eval("(" + oResUserRights + ")") : oResUserRights);
                          //for metadata not associated to PRODIGE data, take geonetwork rights
                          if (this.record.get('isharvested')=="n" && (oRights[this.traitement] || enable)) {
                            tabMetadataDelete.push({
                                  metadata : metadata,
                                  oRights : oRights
                                });
                          }
                          nbDel--;
                          if (nbDel == 0) {
                            doSelectionDelete(metadata.type, tabMetadataDelete);
                          }
                        }
                      });
                }, this);
          } else {
            //doDelete(this.record.get('type') , this.record.get("title"), this.oRights);
            deleteData(catalogue, this.record.get("uuid"), this.record.get("type"), this.oRights, this.record);
          }
        },
        setVisibility : function(catalogue, bInit) {
          if (catalogue.isIdentified() && this.bConnect
              || !catalogue.isIdentified() && this.bSearch) {
            if (bInit) {
              this.show(true);
              return;
            }
            if (this.oMetadataSelect) {
              if (this.oMetadataSelect.length == 0) {
                this.show(true);
              } else {
                this.show();
              }
            } else {
              //action not visible for directory
              if(this.record.get('istemplate')=="s" /*|| this.record.get('type')=="featureCatalog"*/){
                this.hide();
                return;
              }
              var scope = this;
              var enable = Boolean( this.record["geonet:info"].edit ? this.record["geonet:info"].edit : false ) ;
              //for metadata not associated to PRODIGE data, take geonetwork rights
              if (this.record.get('isharvested')=="n" && (this.oRights[this.traitement] || enable)) {
                //@TODO MIGRATION 4.0 (?? visible si il y a des enfants ??)
                /*switch (this.record.get('type') ) {
                  case "series" :
                    ajaxRelatedMetadata("children", "json", this, this.record, function(oRes) {
                      if ( !oRes ){scope.show(false);return;}
                      if ( (oRes.relation instanceof Array ? oRes.relation.length > 0 : oRes.relation["@type"]=="children") )  {
                        scope.show(true);
                            } else {
                        scope.show(false);
                            }
                        });
                  break;
                  default :
                    this.show(bInit);
                  break;
                }*/
                this.show(bInit);
              } else {
                this.show(true);
              }
            }
          } else {
            this.hide();
          }
        },
        getTraitement : function(catalogue) {
          if (catalogue.isIdentified() && this.bConnect
              || !catalogue.isIdentified() && this.bSearch) {
            return this.traitement;
          } else {
            return "";
          }
        }
      });
}

/**
 * return action "Dupliquer"
 */
/*
function getDuplicateProAction(catalogue) {
  return new Ext.ActionPro({
        text : "Dupliquer",
        iconCls : "md-mn-copy",
        traitement : "CMS",
        bConnect : true,
        bSearch : false,
        handler : function() {
          if( $(this.el).prop('disabled') ) return;
          switch (this.record.get('type') ) {
            case "dataset" :
            case "nonGeographicDataset" :
              administration_carto_copyCouche(this.oRights["pk_data"],
                  this.oRights["fmeta_id"], parent.domaines_getCurentDomaine(),
                  parent.domaines_getCurentSousDomaine());
            break;
            case "service" :
              administration_carto_copyMap(this.oRights["pk_data"],
                  this.oRights["pk_stockage_carte"], this.oRights["fmeta_id"]);
            break;
          }
        },
        setVisibility : function(catalogue, bInit) {
          if (catalogue.isIdentified() && this.bConnect
              || !catalogue.isIdentified() && this.bSearch) {
            switch (this.record.get('type') ) {
              case "series" :
                this.hide();
              break;
              case "service" :
                if ( (this.record.get('serviceType') || (this.record.get('serviceType') ? this.record.get('serviceType') : null)) == 'invoke') {
                  this.show((bInit ? true : !this.oRights[this.traitement]));
                } else {
                  this.hide();
                }
              break;
              default :
                this.show((bInit ? true : !this.oRights[this.traitement]));
              break;
            }
          } else {
            this.hide();
          }
        },
        getTraitement : function(catalogue) {
          if (catalogue.isIdentified() && this.bConnect
              || !catalogue.isIdentified() && this.bSearch) {
            return this.traitement;
          } else {
            return "";
          }
        }
      });
}*/

/**
 * return action "Editer" (la fiche de métadonnées)
 */
function getEditNewAction(catalogue) {
/**PRODIGE 40 : remove*/
return;

  return new Ext.ActionPro({
        text : "Éditer",
        iconCls : "fa fa-fw fa-pencil",
        traitement : "CMS",
        bConnect : true,
        bSearch : false,
        handler : function() {
          if( this.disabled ) return;
          var id = this.record.get("id");
          metadataEdit2(id)
          // catalogue.editorWindow.setPosition(0, 0);
        },
        setVisibility : function(catalogue, bInit) {
          if (catalogue.isIdentified() && this.bConnect
              || !catalogue.isIdentified() && this.bSearch) {
            switch (this.record.get('type') ) {
              case "dataset" :
              case "nonGeographicDataset" :
              case "series" :
              case "service" :
                if (bInit) {
                  this.show(true);
                  return;
                }
                $.ajax({
                      crossDomain: true,
                      url : catalogue.services.mdRelation,
                      data : {
                        type : "related",
                        fast : false,
                        uuid : this.record.get("uuid")
                      },
                      success : function(oRes) {
                        var xmlDoc = oRes.responseXML;
                        var tabCatalogue = Ext.DomQuery.jsSelect("relations/relation", xmlDoc);
                        if (tabCatalogue.length > 0) {
                          this.show(!this.oRights[this.traitement]);
                        } else {
                          this.show(!this.oRights[this.traitement]);
                        }
                      },
                      scope : this
                    });
              break;
            }
          } else {
            this.hide();
          }
        },
        getTraitement : function(catalogue) {
          if (catalogue.isIdentified() && this.bConnect
              || !catalogue.isIdentified() && this.bSearch) {
            if (this.record.get('type') || this.record.get('type') == "dataset") {
              return this.traitement;
            } else {
              return "";
            }
          } else {
            return "";
          }
        }
      });
}

/**
 * return action "Créer/Editer le catalogue d'attributs"
 */
/*
function getCatalogueAction(catalogue) {
  return new Ext.ActionPro({
    text : "Créer le catalogue d'attributs",
    iconCls : "md-mn-edit",
    traitement : "CMS",
    action_id : 'catalogue',
    bConnect : true,
    bSearch : false,
    handler : function() {
      if( $(this.el).prop('disabled') ) return;
      var me = this;
      switch (this.record.get('type') ) {
        case "dataset" :
          if (this.catalogue_uuid == -1) {
            // création du catalogue d'attributs
            $.ajax({
              crossDomain: true,
              dataType : 'json',
              url : 'md.create',
              data : angular.extend({
                _content_type : 'json',
                id : catalogue.id_template_catalogue,
                template : 'n',
                child : 'n'
              }, (this.record.get('groupowner') ? {group : this.record.get('groupowner') } : {fullPrivileges : true, group : 0})),
              success : function(results) {
                var catalogue_id = results.id;
                if ( !results.id ) return;
                // get UUID du catalogue d'attributs
                $.ajax({
                  crossDomain: true,
                  url : 'q',
                  data : {
                    _content_type : 'json',
                    _id : catalogue_id,
                    _isTemplate : 'y or n or s',
                    fast : 'index', from : 1, to : 1
                  },
                  success : function(results){
                    if ( !results.metadata )return;
                    me.catalogue_uuid = results.metadata['geonet:info'].uuid;
                    // rattachement en tant que catalogue d'attributs
                    $.ajax({
                      crossDomain: true,
                      url : 'md.processing',
                      data : {
                        id : me.record.get("id"),
                        uuidref : me.catalogue_uuid,
                        process : "fcats-add"
                      },
                      success : function(oRes) {
                        // édition du catalogue d'attributs
                        metadataEdit2(me.catalogue_uuid);
                      }
                    })
                  }
                })
              }
            });
          } else {
            metadataEdit2(me.catalogue_uuid);
          }
        break;
      }
    },
    setVisibility : function(catalogue, bInit) {
      var me = this;
      if ((catalogue.isIdentified() && this.bConnect) || (!catalogue.isIdentified() && this.bSearch)) {
        switch (this.record.get('type') ) {
          case "dataset" :
            if (bInit) {
              this.setText("Créer le catalogue d'attributs");
              this.show(true);
              return;
            }

            ajaxRelatedMetadata("fcat", "xml",
              this,
              this.record,
              function(documentXML) {
                var tabCatalogue = documentXML.getElementsByTagName('info');
                if (tabCatalogue.length > 0) {
                  me.setText("Editer le catalogue d'attributs");
                  me.show(!me.oRights[me.traitement]);
                  me.catalogue_uuid = tabCatalogue[0].getElementsByTagName("uuid")[0].innerHTML;
                } else {
                  me.setText("Créer le catalogue d'attributs");
                  me.show(!me.oRights[me.traitement]);
                  me.catalogue_uuid = -1;
                }
              });
          break;
          case "nonGeographicDataset" :
          case "series" :
          case "service" :
            this.hide();
          break;
        }
      } else {
        this.hide();
      }
    },
    getTraitement : function(catalogue) {
      if ((catalogue.isIdentified() && this.bConnect) || (!catalogue.isIdentified() && this.bSearch)) {
        if (this.record.get('type') || this.record.get('type') == "dataset") {
          return this.traitement;
        } else {
          return "";
        }
      } else {
        return "";
      }
    }
  });
}
*/
/**
 * return sub-menu "Représentation par défaut"
 */
function getRepresentationActions(catalogue) {
  var representationParamAction = new Ext.ActionPro({
        text : "Paramétrer...",
        iconCls : "fa fa-fw fa-wrench",
        handler : function() {
          if( this.disabled ) return;
          administration_carto_paramCarto(catalogue, this.record.get("uuid"));
        }
      });

  var representationInitAction = new Ext.ActionPro({
    text : "Réinitialiser",
    iconCls : "fa fa-fw fa-undo",
    handler : function() {
      if( this.disabled ) return;
      reinitParamCarto(catalogue, this.record.get("uuid"));
    }
  });

  return new Ext.menu.ItemPro({
        text : "Représentation par défaut",
        iconCls : "fa fa-fw fa-globe",
        traitement : "CMS",
        bConnect : true,
        bSearch : false,
        menu : {
          items : [representationParamAction, representationInitAction]
        },
        setVisibility : function(catalogue, bInit) {

          if (catalogue.isIdentified() && this.bConnect
              || !catalogue.isIdentified() && this.bSearch) {

            if (bInit || !this.oRights["visualisable"]) {
              this.hide();
              return;
            }
            switch (this.record.get('type') ) {
              case "dataset" :
                switch (this.oRights["couche_type"]) {
                  case "-3" : // type "table"
                    this.hide();
                    this.menu.items.forEach(function(action, index) {
                          action.hide();
                        });
                  break;
                  default : // other types
                    this.show(!this.oRights[this.traitement]);
                    this.menu.items.forEach(function(action, index) {
                          action.show(!this.oRights[this.traitement]);
                        }, this);
                  break;
                }
              break;
              default :
                this.hide();
                this.menu.items.forEach(function(action, index) {
                      action.hide();
                    });
              break;
            }
          } else {
            this.hide();
          }
        },
        getTraitement : function(catalogue) {
          if (catalogue.isIdentified() && this.bConnect
              || !catalogue.isIdentified() && this.bSearch) {
            if (this.record.get('type') || this.record.get('type') == "dataset") {
              return this.traitement;
            } else {
              return "";
            }
          } else {
            return "";
          }
        }
      });
}

/**
 * Mise en conformité d'un dataset
 */
function conformiteQualite(catalogue, uuid){
	Ext.Msg.confirm(
              'Module Qualité',
                "Vous êtes sur le point d'effectuer une mise en conformité de la fiche de métadonnée vis à vis d'uns standard. Cela inscrira cette confomité dans la fiche de métadonnée. Confirmez-vous cette action ?"
              
              , function(id, value) {
                if (id === 'yes') {
                  $.ajax({
                    crossDomain: true,
                    url : catalogue.URL + "/standards/conformite",
                    data : {
                      uuid : uuid
                    },
                    success : function(oRes) {
                      if (oRes.success) {                        
                          Ext.Msg.alert("Succès",
                              " la mise en conformité a été effectuée.");
                        } else {
                          Ext.Msg.alert("Echec", "Impossible d'effectuer la mise en conformité.");
                      }
                    },
					failure : function() {
					  
					  console.log("failure");
					},	
                    scope : this
                  });
                }
              }, this);
	
}


/**
 * Controle qualité d'un dataset
 */
function controleQualite(catalogue, uuid){
	window.open(catalogue.URL +'/standards/qualite/'+uuid);
}
/**
 * return sub-menu "Qualité"
 */
function getQualiteActions(catalogue) {
  var controleQualiteAction = new Ext.ActionPro({
        text : "Contrôle qualité",
        iconCls : "fa fa-fw fa-list-alt",
        handler : function() {
          if( this.disabled ) return;
          controleQualite(catalogue, this.record.get("uuid"));
        }
      });

  var conformiteAction = new Ext.ActionPro({
    text : "Mise en conformité vis d'un standard",
    iconCls : "fa fa-fw fa-check",
    handler : function() {
      if( this.disabled ) return;
      conformiteQualite(catalogue, this.record.get("uuid"));
    }
  });

  return new Ext.menu.ItemPro({
        text : "Qualité",
        iconCls : "fa fa-fw fa-thumbs-up",
        traitement : "CMS",
        bConnect : true,
        bSearch : false,
        menu : {
          items : [controleQualiteAction, conformiteAction]
        },
        setVisibility : function(catalogue, bInit) {

          if (catalogue.isIdentified() && this.bConnect
              || !catalogue.isIdentified() && this.bSearch) {

            if (bInit || !this.oRights["visualisable"] || this.oRights["PRO_MODULE_STANDARDS"]!="on") {
              this.hide();
              return;
            }
            switch (this.record.get('type') ) {
              case "dataset" :
              case "series" :
                this.show(!this.oRights[this.traitement]);
                    this.menu.items.forEach(function(action, index) {
                          action.show(!this.oRights[this.traitement]);
                        }, this);
              break;
              default :
                this.hide();
                this.menu.items.forEach(function(action, index) {
                      action.hide();
                    });
              break;
            }
          } else {
            this.hide();
          }
        },
        getTraitement : function(catalogue) {
          if (catalogue.isIdentified() && this.bConnect
              || !catalogue.isIdentified() && this.bSearch) {
            if (this.record.get('type') || this.record.get('type') == "dataset") {
              return this.traitement;
            } else {
              return "";
            }
          } else {
            return "";
          }
        }
      });
}

/**
 * return action "Créer une métadonnée enfant"
 */
function getCreateChildAction(catalogue) {
  return new Ext.ActionPro({
        text : "Créer une métadonnée enfant",
        iconCls : "fa fa-fw fa-sitemap",
        traitement : "CMS",
        bConnect : true,
        bSearch : false,
        handler : function() {
          if( this.disabled ) return;
          administration_carto_addFicheEnfant(this.oRights["fmeta_id"], parent
                  .domaines_getCurentDomaine(), parent
                  .domaines_getCurentSousDomaine());
        },
        setVisibility : function(catalogue, bInit) {
          if (catalogue.isIdentified() && this.bConnect
              || !catalogue.isIdentified() && this.bSearch) {
            switch (this.record.get('type') ) {
              case "series" :
                this.show((bInit ? true : !this.oRights[this.traitement]));
              break;
              default :
                this.hide();
              break;
            }
          } else {
            this.hide();
          }
        },
        getTraitement : function(catalogue) {
          if (catalogue.isIdentified() && this.bConnect
              || !catalogue.isIdentified() && this.bSearch) {
            if ((this.record.get('type') ) == "series") {
              return this.traitement;
            } else {
              return "";
            }
          } else {
            return "";
          }
        }
      });
}

/**
 * return action "Administrer la jointure"
 */
function getHarvestedSynchronizationAction(catalogue) {
  return new Ext.ActionPro({
        text : "Synchroniser les données",
        iconCls : "fa fa-fw fa-refresh",
        traitement : "CMS",
        bConnect : true,
        bSearch : false,
        handler : function() {
          if( this.disabled ) return;
          window.open(catalogue.admincartoURL.replace(/\/$/g, '')+'/prodige/synchronisation/admin/'+this.record.get('uuid'));
        },
        setVisibility : function(catalogue, bInit) {
          this.hide();
          if (catalogue.isIdentified() && this.bConnect 
          || !catalogue.isIdentified() && this.bSearch
          ) {
            if (bInit) {
              this.hide();
              return;
            }
            
            if ( (this.record.get('isharvested')=="n" && !this.oRights["SYNCHRONIZE"])
             ||  (this.record.get('type')!="dataset" && this.record.get('type')!="nonGeographicDataset")
             || !eval(this.record["geonet:info"].edit)
            ) {
              this.hide();
            } else {
              this.show(false)
            }
          }
        },
        getTraitement : function(catalogue) {
          if (catalogue.isIdentified() && this.bConnect
              || !catalogue.isIdentified() && this.bSearch) {
            if ((this.record.get('type') ) == "dataset") {
              return this.traitement;
            } else {
              return "";
            }
          } else {
            return "";
          }
        }
      });
}

/**
 * return action "Administrer la jointure"
 */
function getAdminJoinAction(catalogue) {
  return new Ext.ActionPro({
        text : "Administrer la jointure",
        iconCls : "fa fa-fw fa-table",
        traitement : "CMS",
        bConnect : true,
        bSearch : false,
        handler : function() {
          if( this.disabled ) return;
          administration_carto_adminView(this.record.get('id'),
              this.oRights["pk_data"], this.oRights["layer_table"],
              window.location.href, this.oRights["color_theme_id"],
              prodigeConfig.URL);
        },
        setVisibility : function(catalogue, bInit) {
          this.hide();return;
          if (catalogue.isIdentified() && this.bConnect
              || !catalogue.isIdentified() && this.bSearch) {
            if (bInit) {
              this.hide();
              return;
            }
            switch (this.record.get('type') ) {
              case "dataset" :
                switch (this.oRights["couche_type"]) {
                  case "-4" : // type "jointure"
                    this.show(!this.oRights[this.traitement]);
                  break;
                  default : // other types
                    this.hide();
                  break;
                }
              break;
              default :
                this.hide();
              break;
            }
          } else {
            this.hide();
          }
        },
        getTraitement : function(catalogue) {
          if (catalogue.isIdentified() && this.bConnect
              || !catalogue.isIdentified() && this.bSearch) {
            if ((this.record.get('type') ) == "dataset") {
              return this.traitement;
            } else {
              return "";
            }
          } else {
            return "";
          }
        }
      });
}
/**
 * return action "Paramétrer la carte"
 */
function getParamChartAction(catalogue) {
  return new Ext.ActionPro({
        text : "Paramétrer le graphe...",
        iconCls : "fa fa-fw fa-bar-chart",
        traitement : "CMS",
        bConnect : true,
        bSearch : false,
        handler : function() {
          if( this.disabled ) return;
          window.open(catalogue.URL + "/graph/update?uuid="+ this.record.get("uuid"));
        },
        setVisibility : function(catalogue, bInit) {
          if (catalogue.isIdentified() && this.bConnect
              || !catalogue.isIdentified() && this.bSearch) {
            if (bInit) {
              this.hide();
              return;
            }
            var sdom_isset = (this.oRights.sdom_dispose_metadata || false);

            switch (this.record.get('type') ) {
              case "service" :
                if(in_array("chart", this.record.type)){
                //if ((this.record.get('serviceType')) == "invoke") {
                  var enable = this.oRights[this.traitement];
				  
                  //verify rights to edit metadata (creation mode for non harvested data)
                  /*if(!enable && this.record.get("isharvested")=="n"){
                    enable = Boolean( this.record["geonet:info"].edit ? this.record["geonet:info"].edit : false ) ;
                  }*/
                  this.show( !(sdom_isset && enable));
                } else {
                  this.hide();
                }
              break;
              default :
                this.hide();
              break;
            }
          } else {
            this.hide();
          }
        },
        getTraitement : function(catalogue) {
          if (catalogue.isIdentified() && this.bConnect
              || !catalogue.isIdentified() && this.bSearch) {
            if (this.record.get('type') == "service") {
              return this.traitement;
            } else {
              return "";
            }
          } else {
            return "";
          }
        }
      });
}
/**
 * return action "Paramétrer la carte"
 */
function getParamCarteAction(catalogue) {
  return new Ext.ActionPro({
        text : "Paramétrer la carte...",
        iconCls : "fa fa-fw fa-globe",
        traitement : "CMS",
        bConnect : true,
        bSearch : false,
        handler : function() {
          if( this.disabled ) return;
          window.open(catalogue.admincartoURL + "/geosource/init_map/"+ this.record.get("uuid"));
        },
        setVisibility : function(catalogue, bInit) {
          if (catalogue.isIdentified() && this.bConnect
              || !catalogue.isIdentified() && this.bSearch) {
            if (bInit) {
              this.hide();
              return;
            }
            var sdom_isset = (this.oRights.sdom_dispose_metadata || false);
            switch (this.record.get('type') ) {
              case "service" :
                if ((this.record.get('serviceType')) == "invoke") {
                  if(in_array("chart", this.record.type)){
                     this.hide();
                  }else{
                  var enable = this.oRights[this.traitement];
                  //verify rights to edit metadata (creation mode for non harvested data)
                  /*if(!enable && this.record.get("isharvested")=="n"){
                    enable = Boolean( this.record["geonet:info"].edit ? this.record["geonet:info"].edit : false ) ;
                  }*/
                  this.show( !(sdom_isset && enable));
                  }
                } else {
                  this.hide();
                }
              break;
              default :
                this.hide();
              break;
            }
          } else {
            this.hide();
          }
        },
        getTraitement : function(catalogue) {
          if (catalogue.isIdentified() && this.bConnect
              || !catalogue.isIdentified() && this.bSearch) {
            if (this.record.get('type') == "service") {
              return this.traitement;
            } else {
              return "";
            }
          } else {
            return "";
          }
        }
      });
}


/**
 * return action "Renommer la carte"
 */
function getRenameCarteAction(catalogue) {
  return new Ext.ActionPro({
        text : "Renommer la carte...",
        iconCls : "fa fa-fw fa-terminal",
        traitement : "CMS",
        bConnect : true,
        bSearch : false,
        handler : function() {
          if( this.disabled ) return;
          map_change_name(this.oRights["srv"], this.oRights["path"],
              this.oRights["pk_stockage_carte"], document.location.href);
        },
        setVisibility : function(catalogue, bInit) {
          if (catalogue.isIdentified() && this.bConnect
              || !catalogue.isIdentified() && this.bSearch) {
            if (bInit) {
              this.hide();
              return;
            }
            switch (this.record.get('type')) {
              case "service" :
                if (this.record.get('serviceType')
                    && this.record.get('serviceType') == "invoke") {
                  if (this.oRights["carte_type"] == 0) { // carte
                    // interactive
                    this.show(!this.oRights[this.traitement]);
                  } else { // autre carte
                    this.hide();
                  }
                } else {
                  this.hide();
                }
              break;
              default :
                this.hide();
              break;
            }
          } else {
            this.hide();
          }
        },
        getTraitement : function(catalogue) {
          if (catalogue.isIdentified() && this.bConnect
              || !catalogue.isIdentified() && this.bSearch) {
            if (this.record.get('type') == "service") {
              return this.traitement;
            } else {
              return "";
            }
          } else {
            return "";
          }
        }
      });
}

/**
 * return sub-menu "Modifier le statut"
 */
function getUpdateStatutActions(catalogue) {
  function getMode(type, sub_type) {
    var mode = "";
    switch (type) {
      case "dataset" :
        switch (sub_type) {
          case "1" : // vecteur
          case "0" : // raster
          case "-1" : // lot
            mode = "couche";
          break;
          case "-2" : // majic
            mode = "majic";
          break;
          case "-3" : // table
            mode = "table";
          break;
          case "-4" : // vue
            mode = "vue";
          break;
          default :
            mode = "couche";
          break;
        }
      break;
      case "service" :
        mode = "carte"; // enough for service and carte
      break;
      default :
        mode = "couche";
      break;
    }
    return mode;
  }

  var retirerAction = new Ext.ActionPro({
    text : "Retirer",
    iconCls : "fa fa-fw fa-minus",
    traitement : ["PROPOSITION", "PUBLICATION"],
    bConnect : true,
    bSearch : false,
    handler : function() {
      if( this.disabled ) return;
      if (this.oMetadataSelect) {
        var tabMetadataAllowed = [];
        var nbMetadataSelect = this.oMetadataSelect.length;
        this.oMetadataSelect.forEach(function(metadata) {
              $.ajax({
                    crossDomain: true,
                    scope : this,
                    url : catalogue.routes.catalogue.prodige_verify_rights_url,
                    data : {
                      ID : metadata.id,
                      OBJET_TYPE : metadata.type,
                      OBJET_STYPE : (metadata.type == "service"
                          && metadata.serviceType ? metadata.serviceType : ""),
                      TRAITEMENTS : this.traitement.join("|")
                    },
                    success : function(oResUserRights) {
                      var oRights = (typeof oResUserRights=="string" ? eval("(" + oResUserRights + ")") : oResUserRights);
                      if (oRights["statut"] == 2 && oRights[this.traitement[0]]
                          || oRights["statut"] == 3
                          && oRights[this.traitement[0]]
                          && oRights[this.traitement[1]]
                          || oRights["statut"] == 4
                          && oRights[this.traitement[1]]) {
                        metadata.statut = oRights["statut"];
                        tabMetadataAllowed.push(metadata);
                      }
                      nbMetadataSelect--;
                      if (nbMetadataSelect == 0) {
                        var type = this.oMetadataSelect.type;
                        var sub_type = (metadata.serviceType
                            ? metadata.serviceType
                            : oRights["couche_type"]);
                        administration_carto_action_changeStatut(
                            tabMetadataAllowed,
                            1,
                            getMode(type, sub_type),
                            (getUrlParam("modePropositionActif", "off") == "on"),
                            setMetadataDecoration);
                      }
                    }
                  });
            }, this);
      } else {
        administration_carto_changeStatut(this.oRights["fmeta_id"], this.record
                .get("title"), 1, this.mode, setMetadataDecoration);
      }
    },
    setVisibility : function(catalogue, bInit) {
      if ((catalogue.isIdentified() && this.bConnect) || (!catalogue.isIdentified() && this.bSearch)) {
        if (bInit) {
          this.hide();
          return;
        }
        if (this.oMetadataSelect) {
          this.show();
        } else {
          switch (this.oRights["statut"]) {
            case "1" : // en cours
              this.hide();
            break;
            case "2" : // attente validation
              this.show(!this.oRights[this.traitement[0]]);
            break;
            case "3" : // validée
              this.show(!this.oRights[this.traitement[0]]
                  && !this.oRights[this.traitement[1]]);
            break;
            case "4" : // publiée
              this.show(!this.oRights[this.traitement[1]]);
            break;
            default : // incohérent
              this.show(true);
            break;
          }
        }
      } else {
        this.hide();
      }
    }
  });
  var proposerAction = new Ext.ActionPro({
    text : "Proposer",
    iconCls : "fa fa-fw fa-thumbs-o-up",
    traitement : ["CMS_MODE_PROPOSITION"],
    bConnect : true,
    bSearch : false,
    handler : function() {
      if( this.disabled ) return;
      if (this.oMetadataSelect) {
        var tabMetadataAllowed = [];
        var nbMetadataSelect = this.oMetadataSelect.length;
        this.oMetadataSelect.forEach(function(metadata) {
              $.ajax({
                    crossDomain: true,
                    scope : this,
                    url : catalogue.routes.catalogue.prodige_verify_rights_url,
                    data : {
                      ID : metadata.id,
                      OBJET_TYPE : metadata.type,
                      OBJET_STYPE : (metadata.type == "service"
                          && metadata.serviceType ? metadata.serviceType : ""),
                      TRAITEMENTS : this.traitement.join("|")
                    },
                    success : function(oResUserRights) {
                      var oRights = (typeof oResUserRights=="string" ? eval("(" + oResUserRights + ")") : oResUserRights);
                      if (oRights["statut"] == 1 && oRights[this.traitement[0]]) {
                        metadata.statut = oRights["statut"];
                        tabMetadataAllowed.push(metadata);
                      }
                      nbMetadataSelect--;
                      if (nbMetadataSelect == 0) {
                        var type = this.oMetadataSelect.type;
                        var sub_type = (metadata.serviceType
                            ? metadata.serviceType
                            : oRights["couche_type"]);
                        administration_carto_action_changeStatut(
                            tabMetadataAllowed,
                            2,
                            getMode(type, sub_type),
                            (getUrlParam("modePropositionActif", "off") == "on"),
                            setMetadataDecoration);
                      }
                    }
                  });
            }, this);
      } else {
        administration_carto_changeStatut(this.oRights["fmeta_id"], this.record
                .get("title"), 2, this.mode, setMetadataDecoration);
      }
    },
    setVisibility : function(catalogue, bInit) {
      if ((catalogue.isIdentified() && this.bConnect) || (!catalogue.isIdentified() && this.bSearch)) {
        if (bInit) {
          this.hide();
          return;
        }
        if (this.oMetadataSelect) {
          if (getUrlParam("modePropositionActif", "off") == "on") {
            this.show();
          } else {
            this.hide();
          }
        } else {
          switch (this.oRights["statut"]) {
            case "1" : // en cours
              if (getUrlParam("modePropositionActif", "off") == "on") {
                this.show(!this.oRights[this.traitement[0]]);
              } else {
                this.hide();
              }
            break;
            case "2" : // attente validation
            case "3" : // validée
            case "4" : // publiée
              this.hide();
            break;
            default : // incohérent
              this.show(true);
            break;
          }
        }
      } else {
        this.hide();
      }
    }
  });
  var validerAction = new Ext.ActionPro({
    text : "Valider",
    iconCls : "fa fa-fw fa-check",
    traitement : ["CMS", "PROPOSITION"],
    bConnect : true,
    bSearch : false,
    handler : function() {
      if( this.disabled ) return;
      if (this.oMetadataSelect) {
        var tabMetadataAllowed = [];
        var nbMetadataSelect = this.oMetadataSelect.length;
        this.oMetadataSelect.forEach(function(metadata) {
          $.ajax({
                crossDomain: true,
                scope : this,
                url : catalogue.routes.catalogue.prodige_verify_rights_url,
                data : {
                  ID : metadata.id,
                  OBJET_TYPE : metadata.type,
                  OBJET_STYPE : (metadata.type == "service"
                      && metadata.serviceType ? metadata.serviceType : ""),
                  TRAITEMENTS : this.traitement.join("|")
                },
                success : function(oResUserRights) {
                  var oRights = (typeof oResUserRights=="string" ? eval("(" + oResUserRights + ")") : oResUserRights);
                  if (oRights["statut"] == 1 && oRights[this.traitement[0]]
                      || oRights["statut"] == 2 && oRights[this.traitement[1]]) {
                    metadata.statut = oRights["statut"];
                    tabMetadataAllowed.push(metadata);
                  }
                  nbMetadataSelect--;
                  if (nbMetadataSelect == 0) {
                    var type = this.oMetadataSelect.type;
                    var sub_type = (metadata.serviceType
                        ? metadata.serviceType
                        : oRights["couche_type"]);
                    administration_carto_action_changeStatut(
                        tabMetadataAllowed, 3, getMode(type, sub_type),
                        (getUrlParam("modePropositionActif", "off") == "on"),
                        setMetadataDecoration);
                  }
                }
              });
        }, this);
      } else {
        administration_carto_changeStatut(this.oRights["fmeta_id"], this.record
                .get("title"), 3, this.mode, setMetadataDecoration);
      }
    },
    setVisibility : function(catalogue, bInit) {
      if ((catalogue.isIdentified() && this.bConnect) || (!catalogue.isIdentified() && this.bSearch)) {
        if (bInit) {
          this.hide();
          return;
        }
        if (this.oMetadataSelect) {
          this.show();
        } else {
          switch (this.oRights["statut"]) {
            case "1" : // en cours
              if (getUrlParam("modePropositionActif", "off") == "on") {
                validerAction.hide();
              } else {
                this.show(!this.oRights[this.traitement[0]]);
              }
            break;
            case "2" : // attente validation
              this.show(!this.oRights[this.traitement[1]]);
            break;
            case "3" : // validée
            case "4" : // publiée
              this.hide();
            break;
            default : // incohérent
              this.show(true);
            break;
          }
        }
      } else {
        this.hide();
      }
    }
  });
  var publierAction = new Ext.ActionPro({
    text : "Publier",
    iconCls : "fa fa-fw fa-flag",
    traitement : ["PUBLICATION"],
    bConnect : true,
    bSearch : false,
    handler : function() {
      if( this.disabled ) return;
      if (this.oMetadataSelect) {
        var tabMetadataAllowed = [];
        var nbMetadataSelect = this.oMetadataSelect.length;
        this.oMetadataSelect.forEach(function(metadata) {
              $.ajax({
                    crossDomain: true,
                    scope : this,
                    url : catalogue.routes.catalogue.prodige_verify_rights_url,
                    data : {
                      ID : metadata.id,
                      OBJET_TYPE : metadata.type,
                      OBJET_STYPE : (metadata.type == "service"
                          && metadata.serviceType ? metadata.serviceType : ""),
                      TRAITEMENTS : this.traitement.join("|")
                    },
                    success : function(oResUserRights) {
                      var oRights = (typeof oResUserRights=="string" ? eval("(" + oResUserRights + ")") : oResUserRights);
                      if (oRights["statut"] == 3 && oRights[this.traitement[0]]) {
                        metadata.statut = oRights["statut"];
                        tabMetadataAllowed.push(metadata);
                      }
                      nbMetadataSelect--;
                      if (nbMetadataSelect == 0) {
                        var type = this.oMetadataSelect.type;
                        var sub_type = (metadata.serviceType
                            ? metadata.serviceType
                            : oRights["couche_type"]);
                        administration_carto_action_changeStatut(
                            tabMetadataAllowed,
                            4,
                            getMode(type, sub_type),
                            (getUrlParam("modePropositionActif", "off") == "on"),
                            setMetadataDecoration);
                      }
                    }
                  });
            }, this);
      } else {
        administration_carto_changeStatut(this.oRights["fmeta_id"], this.record
                .get("title"), 4, this.mode, setMetadataDecoration);
      }
    },
    setVisibility : function(catalogue, bInit) {
      if ((catalogue.isIdentified() && this.bConnect) || (!catalogue.isIdentified() && this.bSearch)) {
        if (bInit) {
          this.hide();
          return;
        }
        if (this.oMetadataSelect) {
          this.show();
        } else {
          switch (this.oRights["statut"]) {
            case "1" : // en cours
            case "2" : // attente validation
              this.hide();
            break;
            case "3" : // validée
              this.show(!this.oRights[this.traitement[0]]);
            break;
            case "4" : // publiée
              this.hide();
            break;
            default : // incohérent
              this.show(true);
            break;
          }
        }
      } else {
        this.hide();
      }
    }
  });

  return new Ext.menu.ItemPro({
        text : "Modifier le statut",
        iconCls : "fa fa-fw fa-check",
        bConnect : true,
        bSearch : false,
        menu : {
          items : [proposerAction, validerAction, publierAction, retirerAction]
        },
        handler : function() {
        },
        setVisibility : function(catalogue, bInit) {
          if (catalogue.isIdentified() && this.bConnect
              || !catalogue.isIdentified() && this.bSearch) {
            this.show(bInit);
            this.mode = "";
            if (this.oMetadataSelect) {
              if (this.oMetadataSelect.length == 0) {
                this.show(true);
              } else {
                this.show();
              }
            }
            if (this.oRights) {
              var type = this.record.get('type');
              var sub_type = (this.record.get('serviceType')
                  && this.record.get('serviceType')
                  ? this.record.get('serviceType')
                  : this.oRights["couche_type"]);
              this.mode = getMode(type, sub_type);
            }
            this.menu.items.forEach(function(action) {
                  action.mode = this.mode;
                  var old_show = action.show;
                  action.show = function(bDisabled) {
                    this.setDisabled(bDisabled);
                    old_show.call(this);
                    if (this.actionMenu && this.refreshActionMenu) {
                      this.actionMenu.showAt(this.actionMenu.getPosition());
                    }
                  };
                  action.setVisibility(catalogue, bInit);
                }, this);
          } else {
            this.hide();
          }
        }
      });
}

/**
 * @brief ajoute une couche WMS au serveur
 * @param pk_couche
 * @param type_service
 */
function administration_carto_webservice(pk_data, type_service, login, pwd, type_data){
  var   params;
  var   i;
  var   param;
  var   curdom = null;
  var   cursdom = null;
  var   url;
  var   deldata = false;

  params = document.location.search.substr(1).split("&");
  for (i = 0; i < params.length; i++)
  {
    param = params[i].split("=");
    if (param[0] == "domaine")
      curdom = param[1];
    if (param[0] == "sousdomaine")
      cursdom = param[1];
  }
  url = IHM_getBaseUrl();
  if(type_data == 0) //couche
    url += "administration_carto_webservices.php?action="+type_service+"&coucheId=" + pk_data+"&login="+login+"&pass="+pwd+"&type_data="+type_data;
  if(type_data == 1)//carte
    url += "administration_carto_webservices.php?action="+type_service+"&carteId=" + pk_data+"&login="+login+"&pass="+pwd+"&type_data="+type_data;
  if (curdom){
    url += "&domaine=" + escape(curdom);
    if (cursdom)
      url += "&sousdomaine=" + escape(cursdom);
  }
  if(type_data == 0) //couche
    window.location = url;
  if(type_data == 1){
    var iframe = this;
    var ajaxUrl = url;
    var queryParams = {};//{action:"valid",metadataId:metadata_id,statut:statut,mode:mode};
    iframe.tabParams = null;
    iframe.onSuccess = function(responseText){
      var oRes = eval("("+responseText+")");
      if ( oRes.success ) {
        //alert("La carte est publié en WMS.");
        Ext.Msg.alert("Publication WMS", (oRes.enable ? "La carte a été correctement publiée en WMS" : "Le service WMS associé à la carte a été supprimé")) ;
      } else {
        Ext.Msg.alert("Echec", oRes.msg);
      }
    };
    AjaxRequest(ajaxUrl, queryParams, iframe);
  }//carte
}

/**
 * return sub-menu "Gérer l'accès public"
 */
function getPublicAccessActions(catalogue) {
  var WMSAction = new Ext.ActionPro({
        text : "WMS",
        iconCls : "",
        traitement : ["CMS"],
        bConnect : true,
        bSearch : false,
        handler : function() {
          if( this.disabled ) return;
          var type = (this.oRights["carte_type"] == "0" ? 1 : 0);
          var del = this.oRights['carte_wms'] != 0;
          var url = catalogue.URL + "/geosource/wxs/mapAddToWebService/"+this.record.get("uuid")+"?action="+type+"&carteId=" + this.oRights["pk_data"]+"&type_data=1";

          $.ajax({
            crossDomain: true,
            url : url,
            success : function(oRes) {
              if ( oRes.success ) {
                //alert("La carte est publié en WMS.");
                Ext.Msg.alert("Publication WMS", (oRes.enable ? "La carte a été correctement publiée en WMS" : "Le service WMS associé à la carte a été supprimé")) ;
                this.scope.setState(oRes.enable);
              } else {
                Ext.Msg.alert("Echec", oRes.msg);
              }
            },
            scope : this
          });

        },
        setVisibility : function(catalogue, bInit) {

          if (catalogue.isIdentified() && this.bConnect
              || !catalogue.isIdentified() && this.bSearch) {
            if (bInit) {
              this.hide();
              return;
            }
            if (!this.oRights["visualisable"]
                && !(this.oRights["CMS"] && this.oRights["carte_type"] == "0")) {
              this.hide();
              return;
            }
            switch (this.record.get('type')) {
              case "service" :
                switch (this.oRights["carte_type"]) {
                  case "0" : // carte interactive
                    this.show(!this.oRights[this.traitement[0]]);
                    var wms = this.oRights['carte_wms'] == "0" ? 0 : 1;
                    this.setState(wms);
                  break;
                  default :
                    this.hide();
                  break;
                }
              break;

              case "dataset" :
                  this.hide();
              break;

              default :
                this.hide();
              break;
            }
          } else {
            this.hide();
          }
        },
        setState : function(bWMS) {
          this.bWMS = bWMS;
          if (this.bWMS) {
            this.setText("WMS : <span style=\"color:green\">oui</span>");
          } else {
            this.setText("WMS : <span style=\"color:red\">non</span>");
          }
          this.setHandler(this.handler, this);
        }
      });/*
  var WFSAction = new Ext.ActionPro({
        text : "WFS",
        iconCls : "",
        traitement : ["CMS"],
        bConnect : true,
        bSearch : false,
        handler : function() {
          if( $(this.el).prop('disabled') ) return;


          administration_carto_webservice(this.oRights["pk_data"], 'wfs',
              this.oRights["login"], this.oRights["pwd"], 0);
        },
        setVisibility : function(catalogue, bInit) {
          if (catalogue.isIdentified() && this.bConnect
              || !catalogue.isIdentified() && this.bSearch) {
            if (bInit || !this.oRights["visualisable"]) {
              this.hide();
              return;
            }
            switch (this.record.get('type')) {
              case "dataset" :
                switch (this.oRights["couche_type"]) {
                  case "1" : // type "vecteur"
                  case "0" : // type "raster"
                  case "-4" : // type "jointure"
                    this.show(!this.oRights[this.traitement[0]]);
                  break;
                  case "-3" : // type "tabulaire"
                    this.hide();
                  break;
                  default :
                    this.hide();
                  break;
                }
              break;
              default :
                this.hide();
              break;
            }
            this.setState(this.oRights['wfs']);
          } else {
            this.hide();
          }
        },
        setState : function(bWFS) {
          this.bWFS = bWFS;
          if (this.bWFS) {
            this.setText("WFS : <span style=\"color:green\">oui</span>");
          } else {
            this.setText("WFS : <span style=\"color:red\">non</span>");
          }
          this.setHandler(this.handler, this);
        }
      });*/
  var downloadAction = new Ext.ActionPro({
    text : "En Téléchargement libre",
    iconCls : "",
    traitement : ["CMS"],
    bConnect : true,
    bSearch : false,
    handler : function() {

      if( this.disabled ) return;
      Ext.Msg.confirm(
              'Téléchargement libre',
              (this.bDownload ?
                "Vous êtes sur le point de désactiver le téléchargement libre pour cette métadonnée, souhaitez-vous continuer ?"
              :
                (this.record.get("type") == "series"
                  ? "Attention, la publication d'un ensemble de séries de données en téléchargement libre donne accès aux données enfant quel que soit le niveau de publication de celles-ci, souhaitez-vous continuer ?"
                  : "Attention, la publication d'une série de données en téléchargement libre donne accès aux données quels que soient les droits appliqués par ailleurs, souhaitez-vous continuer ?"
                )
              )
              , function(id, value) {
                if (id === 'yes') {
                  $.ajax({
                    crossDomain: true,
                    url : catalogue.URL + "/geosource/setMetaDataDownload",//urlParent + "Services/setMetadataDownload.php",
                    data : {
                      metadata_id : this.record.get("id"),
                      type : this.record.get("type"),
                      mode : (this.bDownload ? "remove" : "add")
                    },
                    success : function(oRes) {
                      if (oRes.update_success) {

                        this.scope.setState(!this.scope.bDownload);
                          Ext.Msg.alert("Succès",
                              "Le téléchargement libre a été "
                                  + (this.scope.bDownload ? "activé" : "désactivé")
                                  + " correctement.");
                        } else {
                        this.scope.setState(this.scope.bDownload);
                        Ext.Msg.alert("Echec", "Impossible "
                                + (this.scope.bDownload
                                    ? "de désactiver"
                                    : "d'activer")
                                + " le téléchargement libre.");
                      }
                    },
                    scope : this
                  });
                }
              }, this);

    },
    setVisibility : function(catalogue, bInit) {
      if ((catalogue.isIdentified() && this.bConnect) || (!catalogue.isIdentified() && this.bSearch)) {
        if (bInit) {
          this.hide();
          return;
        }

        switch (this.record.get('type')) {
          case "dataset" :
		      case "nonGeographicDataset":
            switch (this.oRights["couche_type"]) {
              case "1" : // type "vecteur"
                if (this.oRights["visualisable"]){
                  this.show(!this.oRights[this.traitement[0]]);
                }else {
                  this.hide();
                  return;
                }
                
              case "0" : // type "raster"
              case "-4" : // type "jointure"
              case "-3" : // type "tabulaire"
                this.show(!this.oRights[this.traitement[0]]);
              break;
              default :
                this.hide();
                return;
              break;
            }
          break;
          // type ensemble (ajout BF, prodige 3.4)
          case "series" :
            switch (this.oRights["couche_type"]) {
              case "-1" :
                this.show(!this.oRights[this.traitement[0]]);
              break;
            }
          break;

          default :
            this.hide();
            return;
          break;
        }
        this.setState(this.oRights["download"]);
      } else {
        this.hide();
      }
    },
    setState : function(bDownload) {
      this.bDownload = bDownload;
      if (this.bDownload) {
        this
            .setText("<span title=\"Désactiver le téléchargement libre\">En téléchargement libre : <span style=\"color:green\">oui</span></span>");
      } else {
        this
            .setText("<span title=\"Activer le téléchargement libre\">En téléchargement libre : <span style=\"color:red\">non</span></span>");
      }
      this.setHandler(this.handler, this);
    }
  });
  var domainsPubAction = new Ext.ActionPro({
        text : "Domaines de publication...",
        iconCls : "fa fa-fw fa-list-ul",
        traitement : ["CMS"],
        bConnect : true,
        bSearch : false,
        handler : function() {
          if( this.disabled ) return;
          var type = this.record.get('type');
          switch (type) {
            case "dataset" :
            case "nonGeographicDataset" :
              mode = "couche";
            break;
            case "series" :
              mode = "couche";
            break;
            case "service" :
                if (this.record.get('serviceType') && this.record.get('serviceType') == "invoke") {
                  if(in_array("chart", this.record.type)){
				    mode = "chart";
				  }else{
                  mode = "carte";
				  }
                }else{
                  return false;
                }
            break;
          }
          setDomSdom(catalogue, this.record.get("uuid"), mode);

          /*openPopupDomainsPub(this.record.get("id"), this.record.get("title"),
              type, sub_type);*/
        },
        setVisibility : function(catalogue, bInit) {
          var type = this.record.get('type');
          var serviceType = this.record.get('serviceType');
          var allowedType = false;
          if(type=="dataset" || (type == "service" && serviceType=="invoke") || type == "nonGeographicDataset" || type=="series"){
            allowedType = true;
          }
          if (allowedType && catalogue.isIdentified() && this.bConnect
              || !catalogue.isIdentified() && this.bSearch
              ) {
            if (bInit) {
              this.hide();
              return;
            }
            
            
            var enable = this.oRights[this.traitement[0]];
            //verify rights to edit metadata (creation mode)
            if(!enable && this.record.get("isharvested")=="n"){
              enable = Boolean( this.record["geonet:info"].edit ? this.record["geonet:info"].edit : false ) ;
            }
            this.show(!enable);

          } else {
            
            this.hide();
          }
        }
      });
      
  return new Ext.menu.ItemPro({
        text : "Gérer l'accès public",
        iconCls : "fa fa-fw fa-flag-o",
        bConnect : true,
        bSearch : false,
        menu : {
          items : [WMSAction,/* WFSAction,*/ downloadAction, domainsPubAction
              /*categories*/]
        },
        handler : function() {
        },
        setVisibility : function(catalogue, bInit) {
          if (catalogue.isIdentified() && this.bConnect
              || !catalogue.isIdentified() && this.bSearch) {

            if(this.record.get('istemplate')=="s" || this.record.get('type')=="featureCatalog"){
              this.hide();
              return;
            }
            this.show(bInit || true);
            var bParentDisabled = bInit || true;
            this.menu.items.forEach(function(action, index) {
                  var old_show = action.show;
                  action.show = function(bDisabled) {
                    bParentDisabled = bParentDisabled && bDisabled;
                    this.setDisabled(bDisabled);
                    old_show.call(this, bDisabled);
                    if (this.actionMenu && this.refreshActionMenu) {
                      this.actionMenu.showAt(this.actionMenu.getPosition());
                    }
                  };
                  action.setVisibility(catalogue, bInit);
                }, this);
            this.show(bParentDisabled);
          } else {
            this.hide();
          }
        }
      });
}

/**
 * open ExtJS popup to manage domains/sub-domains publication
 */
function openPopupDomainsPub(fmeta_id, metadata_name, type, sub_type) {
  var treePanel = new Ext.tree.TreePanel({
        root : new Ext.tree.AsyncTreeNode({
              nodeType : "async",
              loader : new Ext.tree.TreeLoader({
                    url : urlParent + "Services/getDomaines.php",
                    baseParams : {
                      fmeta_id : fmeta_id
                    }
                  })
            }),
        rootVisible : false,
        border : true,
        autoScroll : true,
        enableDD : false,
        fieldLabel : "Domaine/Sous-domaine",
        height : 300
      });

  var mode = "";
  var metadataFieldLabel = "";
  switch (type) {
    case "dataset" :
    case "nonGeographicDataset" :
      mode = "couche";
      metadataFieldLabel = "Nom de la série de données";
    break;
    case "series" :
      mode = "couche";
      metadataFieldLabel = "Nom de l'ensemble de série de données";
    break;
    case "service" :
      if (sub_type == "invoke") {
        mode = "carte";
        metadataFieldLabel = "Nom de la carte";
      } else {
        mode = "carte";
        metadataFieldLabel = "Nom du service";
      }
    break;
  }

  var popup = new Ext.Window({
    id : "popupDomainsPub",
    width : "auto",
    height : "auto",
    constrain : true,
    title : "Sélection des domaines/sous-domaines de publication",
    resizable : true,
    maximizable : true,
    items : new Ext.form.FormPanel({
          labelWidth : 150,
          items : [{
                xtype : 'label',
                fieldLabel : metadataFieldLabel,
                width : 200,
                text : metadata_name
              }, treePanel]
        }),
    buttons : [{
      text : 'Valider',
      handler : function() {
        var tabSDom = [];
        selNodes = treePanel.getChecked();
        selNodes.forEach(function(node) {
              var tabTmp = node.id.split("_");
              tabSDom.push(tabTmp[1]);
            });
        if (tabSDom.length == 0) {
          Ext.Msg.alert("Domaine/sous-domaine",
              "Veuillez sélectionner au moins un sous-domaine.");
        } else {
          $.ajax({
            crossDomain: true,
            url : urlParent + "Services/getDomaines.php",
            data : {
              mode : 2
            },
            success : function(oRes) {
              oRes = eval("(" + oRes + ")");
              var bRight = false;
              tabSDom.forEach(function(sDomSelected) {
                   oRes.forEach(function(sDomRight) {
                          if (sDomSelected == sDomRight.id) {
                            bRight = true;
                            return false;
                          }
                        });
                    if (bRight) {
                      return false;
                    }
                  });
              if (bRight) {
                $.ajax({
                  crossDomain: true,
                  url : urlParent + "Services/setMetadataDomSdom.php",
                  data : {
                    id : fmeta_id,
                    mode : mode,
                    "sdom[]" : tabSDom
                  },
                  success : function(oRes) {
                    var oRes = eval("(" + oRes + ")");
                    if (oRes.success) {
                      popup.destroy();
                      location.reload();
                    } else {
                      Ext.Msg
                          .alert("Erreur",
                              "Impossible de mettre à jour les domaines/sous-domaines.");
                    }
                  }
                });
              } else {
                var tabSDomRight = [];
                /*
                 * Ext.each(oRes, function(sDomRight){
                 * tabSDomRight.push(sDomRight.nom); });
                 */
                Ext.Msg
                    .alert(
                        "Domaine/sous-domaine",
                        "Veuillez sélectionner au moins un sous-domaine pour lequel vous avez les droits d'administration."/*
                                                                                                                             * + "
                                                                                                                             * Ceux-ci
                                                                                                                             * sont
                                                                                                                             * listés
                                                                                                                             * ci-dessous :<br/>"+ "<ul><li>"+tabSDomRight.join('</li><li>')+"</li></ul>"
                                                                                                                             */);
              }
            }
          });
        }
      }
    }, {
      text : 'Annuler',
      handler : function() {
        popup.destroy();
      }
    }]
  });
  popup.show();
}

/**
 * return action "Ajouter au panier"
 */
function getAddToPanierAction(catalogue) {
  return new Ext.ActionPro({
    text : "Choisir cette donnée",
    toolTip : "Choisir cette donnée pour préparer le téléchargement ou la covisualisation",
    iconCls : "fa fa-fw fa-star",
    traitement : ["TELECHARGEMENT", "NAVIGATION"],
    bConnect : true,
    bSearch : true,
    bCumulError : false,
    handler : function(action, event, onSuccess) {
      if( this.disabled ) return;
      // action on metadata selection
      if (this.oMetadataSelect) {
        var reader = new Ext.data.JsonReader({
          fields : [{
            name : 'uuid',
            mapping : 'uuid'
          }, {
            name : 'id',
            mapping : 'id'
          }, {
            name : 'title',
            mapping : 'uuid'
          }, {
            name : 'type',
            mapping : 'type'
          }, {
            name : 'isharvested',
            mapping : 'isharvested'
          }, {
            name : 'links',
            mapping : 'links'
          }]
        });
        var store = new Ext.data.JsonStore({
          fields : ['uuid', 'id', 'title', 'type', 'isharvested', 'links'],
          data : this.oMetadataSelect
        });
        var addToPanierAction = getAddToPanierAction(catalogue);
        this.nbAdd = this.oMetadataSelect.length;
        addToPanierAction.bCumulError = true;
        parent.lastError = [];
        this.tabForbidden = [];
        this.tabImpossible = [];
        this.oMetadataSelect.forEach(function(metadata, index) {
          addToPanierAction.setAttr("record", store.getAt(index));
          $.ajax({
                crossDomain: true,
                scope : this,
                url : catalogue.routes.catalogue.prodige_verify_rights_url,
                data : {
                  ID : metadata.id,
                  OBJET_TYPE : metadata.type,
                  OBJET_STYPE : (metadata.type == "service"
                      && metadata.serviceType ? metadata.serviceType : ""),
                  TRAITEMENTS : addToPanierAction.getTraitement(catalogue)
                },
                success : function(oResUserRights) {
                  var oRights = (typeof oResUserRights=="string" ? eval("(" + oResUserRights + ")") : oResUserRights);
                  addToPanierAction.setAttr("record", store.getAt(index));
                  addToPanierAction.setAttr("oRights", oRights);
                  addToPanierAction.setVisibility(catalogue);
                  if (addToPanierAction.isHidden()) {
                    this.tabImpossible.push(metadata.title);
                    this.onAddFinished();
                  } else if (addToPanierAction.isDisabled()) {
                    this.tabForbidden.push(metadata.title);
                    this.onAddFinished();
                  } else {
                    addToPanierAction.handler(null, null, Ext.createDelegate(this.onAddFinished, this));
                  }
                }
              });
        }, this);
      }
      // action on one metadata
      else {
        var type = this.record.get("type");
        switch (type) {
          case "dataset" :
          case "nonGeographicDataset" :
          case "series" :
            panier_ajouter(this.record, this.bCumulError);
            if (onSuccess)
              onSuccess();
          break;
        /* case "series" :
            ajaxRelatedMetadata(null,null,
              this,
              this.record,
              function(json) {
                if ( !json ) return;
                alert('TODO : geosource/apps_extend/actions.js::getAddToPanierAction / ajax / relatedMetadata');
                console.error('TODO : geosource/apps_extend/actions.js::getAddToPanierAction / ajax / relatedMetadata', json);
                return;

                var xmlDoc = oRes.responseXML;
                var tabChildren = Ext.DomQuery.jsSelect("relations/relation", xmlDoc);
                if (!this.bCumulError) {
                  parent.lastError = [];
                }
                for (var i = 0; i < tabChildren.length; i++) {
                  parent.panier_ajouterElement(
                    Ext.DomQuery.selectValue("uuid", tabChildren[i])
                  , Ext.DomQuery.selectValue("id", tabChildren[i])
                  , Ext.DomQuery.selectValue("title", tabChildren[i])
                  , this.record, true);
                }
                if (!this.bCumulError && parent.lastError.length > 0) {
                  if (parent.lastError.length == 1) {
                    Ext.Msg.alert('Panier', 'La couche '
                            + parent.lastError.join(', ')
                            + ' est déjà présente dans le panier.');
                  } else {
                    Ext.Msg.alert('Panier', 'Les couches '
                            + parent.lastError.join(', ')
                            + ' sont déjà présentes dans le panier.');
                  }
                  parent.lastError = [];
                }
                if (onSuccess)
                  onSuccess();
              },
              null
            );
          break;*/
        }
      }
    },
    setVisibility : function(catalogue, bInit) {
      if ((catalogue.isIdentified() && this.bConnect) || (!catalogue.isIdentified() && this.bSearch)) {
        // visibility on metadata selection action
        if (this.oMetadataSelect) {
          this.toolTip = "Ajouter la sélection aux données choisies pour préparer le téléchargement ou la covisualisation";
          if (this.oMetadataSelect.length > 0) {
            var bDisabled = true;
            this.oMetadataSelect.forEach(function(metadata) {
                  if (metadata.type != "service") {
                    bDisabled = false;
                    return false;
                  }
                });
            this.show(bDisabled);
          } else {
            this.show(true);
          }
        }
        // visibility on one metadata action
        else {
          if(this.record.get('istemplate')=="s" || this.record.get('type')=="featureCatalog"){
            this.hide();
            return;
          }
          var type = this.record.get("type");
          switch (type) {
            case "dataset" :
            case "nonGeographicDataset" :
            case "series" :
              switch (this.record.get("isharvested")) {
                case "y" :
                  var tabLinks = this.record.get("links");
                  if (tabLinks.length == 0) {
                    this.hide();
                  } else {
                    this.show(bInit);
                  }
                break;
                case "n" :
                  this.show(bInit || !(this.oRights[this.traitement[0]] || this.oRights[this.traitement[1]]));
                break;
              }
            break;
            /*case "series" :
              this.show(bInit || !(this.oRights[this.traitement[0]] || this.oRights[this.traitement[1]]));
            break;*/
            case "service" :
              this.hide();
            break;
          }
        }
      } else {
        this.hide();
      }
    },
    getTraitement : function(catalogue) {
      if ((catalogue.isIdentified() && this.bConnect) || (!catalogue.isIdentified() && this.bSearch)) {
        if (this.oMetadataSelect) {
          return this.traitement;
        } else {
          var type = this.record.get("type");
          if ( ["series", "dataset", "nonGeographicDataset"].indexOf(type)!=-1 && this.record.get("isharvested") == "n") {
            if (this.traitement instanceof Array) {
              return this.traitement.join("|");
            } else {
              return this.traitement;
            }
          } else {
            return "";
          }
        }
      } else {
        return "";
      }
    },
    onAddFinished : function() {
      this.nbAdd--;
      if (this.nbAdd == 0
          && (parent.lastError.length > 0 || this.tabForbidden.length > 0 || this.tabImpossible.length > 0)) {
        var tabMsg = [];
        if (parent.lastError.length == 1) {
          tabMsg.push("La couche " + parent.lastError.join(", ")
              + " est déjà présente dans les données choisies.");
        } else if (parent.lastError.length > 1) {
          tabMsg.push("Les couches " + parent.lastError.join(", ")
              + " sont déjà présentes dans les données choisies.");
        }
        if (this.tabForbidden.length == 1) {
          tabMsg
              .push("Vous ne possédez pas les droits suffisants pour télécharger la couche suivante :<br/>"
                  + this.tabForbidden.join(", "));
        } else if (this.tabForbidden.length > 1) {
          tabMsg
              .push("Vous ne possédez pas les droits suffisants pour télécharger les couches suivantes :<br/>"
                  + this.tabForbidden.join(", "));
        }
        if (this.tabImpossible.length == 1) {
          tabMsg.push("La couche " + this.tabImpossible.join(", ")
              + " n'est pas téléchargeable.");
        } else if (this.tabImpossible.length > 1) {
          tabMsg.push("Les couches " + this.tabImpossible.join(", ")
              + " ne sont pas téléchargeables.");
        }
        if (tabMsg.length > 0) {
          Ext.Msg.alert('Panier', tabMsg.join("<br/><br/>"));
        }
        parent.lastError = [];
        this.tabForbidden = [];
        this.tabImpossible = [];
      }
    }
  });
}

/**
 * return action "Visualiser"
 */
function getVisualiserAction(catalogue) {
  return new Ext.ActionPro({
    text : "Visualiser",
    toolTip : "Voir les données sur l'interface cartographique",
    iconCls : "fa fa-fw fa-globe",
    traitement : "NAVIGATION",
    bConnect : true,
    bSearch : true,
    handler : function() {
      if( this.disabled ) return;
      switch (this.record.get('type')) {
        case "dataset" :

          //addLog('WMS', (typeof(this.record.get("title"))!="undefined" ? this.record.get("title").replace(",",";") : "")+","+this.record.get("id"));
          var isharvested = this.record.get("isharvested");
          switch (isharvested) {
            case "y" :
              //window.open(urlParent + "consultation_wms.php?IDT="+ this.record.get("id") + this.paramVisu);
              window.open(catalogue.URL + "/geosource/consultationWMS?IDT="+ this.record.get("id") + this.paramVisu);
            break;
            default :
            case "n" :
              //window.open(urlParent + "consultation_wms.php?IDT="+ this.record.get("id"));
              window.open(catalogue.URL + "/geosource/consultationWMS?IDT="+ this.record.get("id"));
            break;
          }
        break;
        case "series" :
          // nothing to do
        break;
        case "service" :
          if(in_array("chart", this.record.type)){
            //chart link
             window.open(catalogue.URL + "/graph/readOnly?uuid=" + this.record.get("uuid"));
          }
          else if ((this.record.get('serviceType') == 'invoke')) {
            // statistiques de consultation de carte
            /*
             * $.ajax({ url:urlParent+"consultation_addLog.php",
             * params:{logFile:"ConsultationCarte", objectName :
             * this.record.get("title").replace(",",
             * ";")+","+this.record.get("id")} });
             */
            //addLog('ConsultationCarte', (typeof(this.record.get("title"))!="undefined" ? this.record.get("title").replace(",",";") : "")+","+this.record.get("id"));
            var isharvested = this.record.get("isharvested");
            switch (isharvested) {
              case "y" :
                var tabLinks = this.record.get("links");
                if (tabLinks.length > 0) {
                  window.open(tabLinks[0].href);
                }
              break;
              case "n" :
                //alert('TODO apps_extend/actions.js/getVisualiserAction::handler(service|map, n) => controller catalogue consultation.php');return;
                //window.open(urlParent + "consultation.php?id=" + this.record.get("id"));
                window.open(catalogue.URL + "/geosource/consultation?id=" + this.record.get("id"));
              break;
            }
          } else {
            var tabLinks = this.record.get("links");
            if (tabLinks.length > 0) {
              window.open(tabLinks[0].href);
            }
          }
        break;
      }
    },
    setVisibility : function(catalogue, bInit) {
      if ((catalogue.isIdentified() && this.bConnect) || (!catalogue.isIdentified() && this.bSearch)) {
        if (this.record.get('istemplate')=="s" || this.record.get('type')=="featureCatalog"){
          this.hide();
          return;
        }
        switch (this.record.get('type')) {
          case "dataset" :
            if (this.record.get('spatialRepresentationType') == "Tabulaire") {
              this.hide();
            } else {
              switch (this.record.get("isharvested")) {
                case "y" :
                  this.paramVisu = "";
                  if (bInit) {
                    this.hide();
                  } else {
                        var me = this;
                    ajaxServicesMetadata(this, this.record
                    , function(oRes) {
                        /*
                         * var xmlDoc = oRes.responseXML; var tabService =
                         * Ext.DomQuery .jsSelect( "relations/relation",
                         * xmlDoc); if (tabService.length > 0) { // pas de //
                         * vérification de // droits pour les // données
                         * associées // à un service de // covisu WMS/WFS
                         * this.paramVisu = "&layerTitle=" + this.record
                         * .get("title") + "&Mode=WMS&uuidService="; for ( var i =
                         * 0; i < tabService.length; i++) { this.paramVisu +=
                         * Ext.DomQuery .selectValue( "uuid", tabService[i]) +
                         * "|"; } this.paramVisu += "&uuidData=" + this.record
                         * .get("uuid"); this.show(); } else {
                         */
                        var tabLinks = me.record.get("links");
                        if (tabLinks.length > 0) {
                          var oLink = {};
                          tabLinks.forEach(function(link) {
                            if (link.protocol && link.protocol != "" && new RegExp(/^OGC:WMS/i).test(link.protocol)) {
                              if (link.href && link.href != "" && link.name && link.name != "") {
                                oLink.wms = "&layerTitle="
                                    + me.record.get("title")
                                    + "&Mode=WMS&protocol=GetMap&connection="
                                    + link.href + "&layer=" + link.name;
                              }
                            }
                            if (link.protocol && link.protocol != "" && new RegExp(/^OGC:WFS/i).test(link.protocol)) {
                              if (link.href && link.href != "" && link.name && link.name != "") {
                                oLink.wfs = "&layerTitle="
                                    + me.record.get("title")
                                    + "&Mode=WMS&protocol=GetFeatures&connection="
                                    + link.href + "&layer=" + link.name;
                              }
                            }
                          }, me);
                          if (oLink.wms) {
                            me.paramVisu = oLink.wms;
                            me.show();
                          } else if (oLink.wfs) {
                            me.paramVisu = oLink.wfs;
                            me.show();
                          } else {
                            me.hide();
                          }
                        } else {
                          me.hide();
                        }
                        // }
                      });
                  }
                  this.setHandler(this.handler, this);
                break;
                case "n" :
                  this.show((bInit ? true : (this.oRights["visualisable"]
                      ? !this.oRights[this.traitement]
                      : true)));
                break;
              }
            }
          break;
          case "series" :
            this.hide();
          break;
          case "nonGeographicDataset" :
            this.hide();
          break;
          case "service" :
            if ((this.record.get('serviceType') == 'invoke')) {
              if(in_array("chart", this.record.type)){
				  this.hide();
			  }else{
              switch (this.record.get("isharvested")) {
                case "y" :
                  var tabLinks = this.record.get("links");
                  if (tabLinks.length > 0) {
                    this.show();
                  } else {
                    this.hide();
                  }
                break;
                case "n" :
                  this.show((bInit ? true : !this.oRights[this.traitement]));
                break;
              }
             }
            } else {
              var tabLinks = this.record.get("links");
              if (tabLinks.length > 0) {
                this.show();
              } else {
                this.hide();
              }
            }
          break;
        }
      } else {
        this.hide();
      }
    },
    getTraitement : function(catalogue) {
      if ((catalogue.isIdentified() && this.bConnect) || (!catalogue.isIdentified() && this.bSearch)) {
        if (this.record.get('type') == "service"
            || this.record.get('type') == "dataset"
            && this.record.get("isharvested") == "n") {
          return this.traitement;
        } else {
          return "";
        }
      } else {
        return "";
      }
    }
  });
}


/**
 * return action "Importer les données"
 */
function getVisualiserChartAction(catalogue) {
  return new Ext.ActionPro({
        text : "Visualiser le graphe...",
        iconCls : "fa fa-fw fa-bar-chart",
        traitement : "NAVIGATION",
        bConnect : true,
        bSearch : false,
        handler : function() {
          if( this.disabled ) return;
          window.open(catalogue.URL + "/graph/readOnly?uuid="
          + this.record.get("uuid"));
        },
        setVisibility : function(catalogue, bInit) {
          if (catalogue.isIdentified() && this.bConnect
              || !catalogue.isIdentified() && this.bSearch) {
            if (bInit) {
              this.hide();
              return;
            }
			this.table = "";
		    switch (this.record.get('type') ) {
		      case "service" :
		  
			  if(in_array("chart", this.record.type)){
			    var disable = !this.oRights[this.traitement];
			    //this.table = this.oRights["tableName"];
			    this.show(disable);
			  } else {
			    this.hide();
			  }
			  break;
			  default :
			    this.hide();
			  break;
		    }
          } else {
            this.hide();
          }
        },
        getTraitement : function(catalogue) {
          if (catalogue.isIdentified() && this.bConnect
              || !catalogue.isIdentified() && this.bSearch) {
            return this.traitement;
          } else {
            return "";
          }
        }
      });
}


/**
 * return action "Export PDF Complet"
 */
function getExportPDFCompletAction(catalogue) {
  return new Ext.ActionPro({
    text : "Export PDF complet (ZIP)",
    toolTip : "Exporter la fiche de métadonnée et ses métadonnées asssociées au format PDF",
    iconCls : "fa fa-fw fa-file-zip-o",
    bConnect : false,
    bSearch : true,
    handler : function() {
    if( this.disabled ) return;
    //alert('TODO apps_extend/actions.js/getExportPDFCompletAction::handler => controller catalogue Services/getMetadataPDFComplet.php');return;
      window.location = catalogue.URL + "/metadata/pdf?uuid="//"Services/getMetadataPDFComplet.php?uuid="
          + this.record.get("uuid");
    },
    setVisibility : function(catalogue, bInit) {
      this.show();
    }
  });
}
/**
 * return action
 */
function getSubMenuAction(catalogue, texte, url, target) {
  return new Ext.ActionPro({
        text : texte,
        bConnect : false,
        bSearch : true,
        handler : function() {
          if (target == "_blank")
            window.open(url);
          else
            window.location = url;
        },
        setVisibility : function(catalogue, bInit) {
          if (bInit)
            this.show();
          else
            this.hide();
        }
      });
}

function ajaxRelatedMetadata(type, content_type, scope, metadata, successFn, failureFn){
  $.ajax({
    crossDomain: true,
    dataType: "json",
    accepts: {
        text: "application/json"
    },
    url : '/geonetwork/srv/api/records/'+metadata.get("uuid")+'/related',
    data : {
      type : type||["siblings", "associated", "related"]
    },
    success : successFn,
    failure : failureFn,
    scope : scope
  });
}
function ajaxServicesMetadata(scope, metadata, successFn, failureFn){
  $.ajax({
    crossDomain: true,
    dataType: "json",
    accepts: {
        text: "application/json"
    },
    url : '/geonetwork/srv/api/records/'+metadata.get("uuid")+'/related',
    data : {
      type : "services"
    },
    success : successFn,
    failure : failureFn,
    scope : scope
  });
}

/**
 * @version 3.2
 * effectue un appel Ajax
 * @param ajaxUrl        URL de l'appel
 * @param queryParams    paramètres de l'appel
 * @param iframe         objet contenant les fonctions "onSuccess" ainsi que les paramètres "tabParams" associés
 * @param method         méthode de l'appel ("GET" par défaut)
 */
function AjaxRequest(ajaxUrl, queryParams, iframe, method, async){
  if ( typeof method=="undefined" ) method = 'GET';
  if ( typeof async=="undefined" ) async = true;
  switch(ajaxUrl){
    case "Services/getUserRights.php" : ajaxUrl = prodigeConfig.routes.catalogue.prodige_verify_rights_url; break;
  }
  $.ajax({
                dataType: "json",
                accepts: {
                    text: "application/json"
                },
    async : async,
    crossDomain: true,
    url : ajaxUrl ,
    data : queryParams,
    method : ( method ? method : 'GET' ),
    success : function(result){
      var fn;
      if ( typeof iframe.onSuccess == 'function' ) {
        fn = iframe.onSuccess;
        if ( !iframe.onSuccess.createDelegate ) {
          fn = function(){
            return iframe.onSuccess.apply(this, arguments)
          }
        }
        var response = (typeof result == "string" ? result.replace(/^\((.*)\)$/g, '$1') : result);
        fn.createDelegate(this, ( iframe.tabParams && iframe.tabParams.concat && typeof iframe.tabParams.concat == 'function' ? iframe.tabParams.concat(response) : [response] )).call();
      }
    },
    failure : function(result){
      console.log("failedAjax")
      console.log(result);
    }
  });
}
Function.prototype.createDelegate = function(obj, args, appendArgs){
  var method = this;
  return function() {
    var callArgs = args || arguments;
    if (appendArgs === true){
      callArgs = Array.prototype.slice.call(arguments, 0);
      callArgs = callArgs.concat(args);
    }else if (typeof appendArgs === 'number' && isFinite(appendArgs)){
      callArgs = Array.prototype.slice.call(arguments, 0); // copy arguments first
      var applyArgs = [appendArgs, 0].concat(args); // create method call params
      Array.prototype.splice.apply(callArgs, applyArgs); // splice them in
    }
    return method.apply(obj || window, callArgs);
  };
}



    function getMetadataEditURL(id, create, group, child, isTemplate, schema){
        var url = 'catalog.edit#/';
        if (create) {
          if (id) {
            if (child) {
              url += 'create?childOf=' + id;
            } else {
              url += 'create?from=' + id;
            }
          } else {
            url += 'create';
          }
        } else {
          url += 'metadata/' + id;
        }
        return url;
    }
    function metadataEdit2(id, create, group, child, isTemplate, schema){
        var url = getMetadataEditURL(id, create, group, child, isTemplate, schema);
        window.open(url, 'metadata_'+id);
    };


    /**
     * @brief open the map in a simplified mode to edit the default representation of the layer
     * @param action : url to open
     */
    function  administration_carto_paramCarto(catalogue, uuid)
    {
      //open map in popup
      parent.administration_carto_win_cartodynamique = window.open(catalogue.URL+"/geosource/layerAddToMap/"+uuid/*, "Cartographie_dynamique", "directories=0, fullscreen=yes, menubar=0, status=1, toolbar=0, resizable=1"*/);
    }

    /**
     * @brief open in new window import data interface
     * @param uuid metadata uuid
     */
    function  import_data(catalogue, uuid)
    {
       window.open(catalogue.admincartoURL+"/prodige/importdata/manage/"+uuid/*, "Cartographie_dynamique", "directories=0, fullscreen=yes, menubar=0, status=1, toolbar=0, resizable=1"*/);
    }

    function setDomSdom (catalogue, uuid, mode)
    {
      window.open(catalogue.URL+"/geosource/setDomsdom/"+uuid+"?mode="+mode/*, "Cartographie_dynamique", "directories=0, fullscreen=yes, menubar=0, status=1, toolbar=0, resizable=1"*/);
    }

    function deleteData(catalogue, uuid, type, oRights, record){
      confirmActionDelete = function(id, value){
        if (id === 'yes') {
        	var msg = $('body').append('<div id="msg-remove" class="modal fade" tabindex="-1" role="dialog" >  ' +
        			'<div  class="modal-dialog" role="document">' +
        			'<div class="modal-content">' +
        			'<div class="modal-body" ng-bind-html="body">Suppression en cours...</div>')
        	var closeFn = Ext.Msg.info('Suppression', "Suppression en cours...");
          $.ajax({
            crossDomain: true,
            url : catalogue.URL+"/geosource/deleteMetaData/"+ uuid,
            success : function(oResUserRights) {
              $("#msg-remove").remove();
              var msg = Ext.Msg.alert('Suppression', "La ressource a été supprimée");
              setTimeout(function(){
              	msg && msg.close && msg.close();
              // relancer la recherche pour actualiser les résultats
              $('.gn-top-search button[data-ng-click="triggerSearch()"]').click();
              }, 3000);
            },
            failure : function() {
              $("#msg-remove").remove();
              console.log("failure");
            }
          });
        }

      };
      var name =  record.title || record.defaultTitle;
      if(type=="dataset" && (oRights["couche_type"]==1 || oRights["couche_type"]==0 || oRights["couche_type"]==4)){
        //first verify maps
        $.ajax({
          crossDomain: true,
          url : catalogue.URL+"/geosource/canDeleteMetaData/"+ uuid,
          success : function(response) {
            var message = [];
            if ( response.maps.length ){
              message.push('La métadonnée sera supprimée des cartes suivantes :' +
                  '<ul><li>'+response.maps.join('</li><li>')+'</li></ul>');
            } else {
              message.push("Aucune carte ne contient la métadonnée.");
            }
            if ( response.joins.length ){
              message.push('Les vues de base de données suivantes seront supprimées :' +
                  '<br/><em>(Les configurations associées restent inchangées)</em>' +
                  '<ul><li>'+response.joins.join('</li><li>')+'</li></ul>');
            } else {
              message.push("Aucune vues de jointures ne contient la métadonnée.");
            }
            Ext.Msg.confirm('Suppression de la métadonnée "'+name+'"',
                ('Examen de l\'utilisation des couches de la métadonnée "'+name+'" dans les cartes et les jointures :' +
                '<ul><li>'+message.join('</li><li>')+'</li></ul>' +
                '<br/><b>Souhaitez-vous poursuivre la suppression ?</b>'), confirmActionDelete, this);

            return;
            var tabmaps = eval(oResUserRights);
            if(tabmaps.length == 0){
              Ext.Msg.confirm("Suppression de couche", "Aucune carte ne contient la couche "+name+", souhaitez-vous poursuivre la suppression ?", confirmActionDelete, this);
            }else{
              var strMaps ="";
              for (var i=0; i<tabmaps.length;i++){
                strMaps+= tabmaps[i]+", ";
              }
              strMaps = strMaps.slice(0,-2)+".";
              Ext.Msg.confirm("Suppression de couche", "La couche "+name+" est contenue dans les cartes suivantes :" +
                                  strMaps+ "La couche sera supprimée des cartes. Souhaitez-vous poursuivre la suppression ?", confirmActionDelete, this);
            }

          },
          failure : function() {
            console.log("failure");
          }
        });
      }else{
      	Ext.Msg.confirm('Suppression de la métadonnée "'+name+'"',
             "Souhaitez-vous poursuivre la suppression ?", confirmActionDelete, this);
      }



    }

    /**
     * Réinitialisation de la représentation par défaut
     */
    function reinitParamCarto(catalogue, uuid)
    {
      $.ajax({
        crossDomain: true,
        url : catalogue.URL+"/geosource/mapDelete/"+ uuid,
        success : function(oResUserRights) {
          //Ext.Msg.alert("La représentation par défaut a été réinitialisée");
          Ext.Msg.alert('Représentation par défaut', "La représentation par défaut a été réinitialisée", function(){});
        },
        failure : function() {
          console.log("failure");
        }
      });
    }

/**
 * Open the UserDetails prodige form in a popup
 */
function openUserDetails()
{
  var width   = 680;
  var left    = (screen.width/2)-(width/2);
  window.open(prodigeConfig.routes.catalogue.geosource_user_details, "Ma fiche utilisateur", "height=400,top=300,width="+width+",left="+left+", directories=0, fullscreen=yes, menubar=0, status=1, toolbar=0, resizable=1");
}

/**
 * @param logFile
 * @param objectName
 */
function addLog(logFile, objectName)
{
  $.get(
    __PRODIGE_CATALOGUE_CONFIG_URL__+'/prodige/add_log',
    { logFile:logFile, objectName : objectName }
  )
  .fail(function(){
    console.log("Erreur d'ecriture du log '"+logFile+"'");
  });
}
